// The Arduino UNO has a jumper pad you can cut to disable the reset that occurs when opening serial communication

#include <EEPROM.h>

#define MIN_PACKET 1
#define MAX_PACKET 100
const char PACKET_START = '<';
const char PACKET_DELIM = ',';
const char PACKET_DELIMS[] = ",";
const char PACKET_END = '>';

// Pins 0 and 1 are serial
#define PIN_MIN 2
#define PIN_MAX 13

void report_pin(int pin, bool state)
{
  Serial.print(PACKET_START);
  Serial.print("d");
  Serial.print(PACKET_DELIM);
  Serial.print(pin);
  Serial.print(PACKET_DELIM);
  if (state)
    Serial.print("1");
  else
    Serial.print("0");
  Serial.println(PACKET_END);
}

void setup() 
{
  Serial.begin(9600);

  Serial.print(PACKET_START);
  Serial.print("boot");
  Serial.println(PACKET_END);

  // Restore previous state if power was lost
  for (int i = PIN_MIN; i <= PIN_MAX; i++)
  {
    int state = EEPROM.read(i);
    pinMode(i, OUTPUT);
    digitalWrite(i, state);
    report_pin(i, state);
  }
}


void loop() 
{
  char packet[MAX_PACKET];
  int packet_len = 0;

  // Wait for a packet start character
  while(Serial.read() != PACKET_START) {}

  // Read the whole packet
  packet_len = Serial.readBytesUntil(PACKET_END, packet, MAX_PACKET-1);
  packet[packet_len] = 0; // Null terminate for strtok

  if (packet_len < MIN_PACKET)
    return;

  char* cmd = strtok(packet, PACKET_DELIMS);

  if (strcmp(cmd, "dout") == 0)
  {
    int pin = atoi(strtok(NULL, PACKET_DELIMS));
    int value = atoi(strtok(NULL, PACKET_DELIMS));

    if (pin >= PIN_MIN && pin <= PIN_MAX)
    {
      if (value)
        value = HIGH;
      else
        value = LOW;
      
      digitalWrite(pin, value);
      report_pin(pin, value);
      EEPROM.update(pin, value);
    
      return;
    }
  }
  else if (strcmp(cmd, "zeroall") == 0)
  {
    for (int i = PIN_MIN; i <= PIN_MAX; i++)
    {
      digitalWrite(i, LOW);
      report_pin(i, LOW);
      EEPROM.update(i, LOW);
    }

    return;
  }
  else if (strcmp(cmd, "din") == 0)
  {
    int pin = atoi(strtok(NULL, PACKET_DELIMS));
    
    if (pin >= PIN_MIN && pin <= PIN_MAX)
    {
      report_pin(pin, digitalRead(pin));
      return;
    }
  }

  // If we made it here, something was wrong with the packet
  Serial.print(PACKET_START);
  Serial.print("error");
  Serial.print(PACKET_DELIM);
  Serial.print(cmd);
  Serial.println(PACKET_END);
  
  
  
}

