#!/usr/bin/env python3

import serial
import time
import sys
import os

'''
The first argument should be the port the Prologix controller is attached to.
Example:
	./gpibcheck.py /dev/ttyPrologix
'''

if len(sys.argv) < 2:
	port = '/dev/ttyPrologix'
	print("A port was not provided as an argument, so /dev/ttyPrologix is assumed")
else:	
	port = str(sys.argv[1])
assert os.path.exists(port), "The port %s does not exist!" % (port)

s = serial.Serial(port=port, baudrate=19200, timeout=0.05)

s.write(b'++mode 1\n++auto 0\n++read_tmo_ms 200\n++ifc\n')
time.sleep(0.5)

# Check if the Prologix is responding to a version request
def verify_live(first_run=False):
	s.write(b'++ver\n')
	ver_str = s.readline().decode().strip()
	if 'Prologix' not in ver_str:
		if first_run:
			sys.exit("Prologix controller is not responding!  Check that the serial port is correct and that the device is plugged in.  Try disconnecting and reconnecting the device.")
		else:
			sys.exit("Prologix controller has stopped responding!  This is usually indicative of a device on the GPIB bus causing interference.  Make sure all GPIB devices on the bus are powered on (even if not in use), and check that they all have unique GPIB addresses.  Try rebooting all devices on the bus (Agilent power supplies especially, they occasionally get themselves into a bad state).")
	elif first_run:
		print(ver_str)
	
verify_live(first_run=True)
print("Checking bus...")

# Request and print the identity string from each address
for addr in range(31):
	
	print("%i: " % addr, end='')
	
	s.write(b'++addr %i\n++clr\n*IDN?\n++read eoi\n' % addr)
	time.sleep(0.3)
	
	print(s.readline().decode().strip())

	verify_live()
	
print("Finished!")
