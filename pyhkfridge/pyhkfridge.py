#!/usr/bin/env python3

'''
This script is the main extry point to pyhkfridge.  Execute it with no 
arguments to print the help.
'''

import sys
import logging

from pyhkfridgelib.settings import *
sys.path.append(COMMON_CODE_DIR)

# Configure logging
log_filename = os.path.join(APP_LOG_BASE_FOLDER, APP_LOG_FILENAME_INIT)
logging.basicConfig(filename=log_filename, format=APP_LOG_FORMAT, level=logging.DEBUG)
screen_handler = logging.StreamHandler() 
screen_handler.setFormatter(logging.Formatter(APP_LOG_FORMAT))
logging.getLogger().addHandler(screen_handler)  # Print to screen as well
logging.info("Starting PyHK Fridge Manager " + VERSION_STR)

import checkdep # Verifies depenencies, comment out to override

import setproctitle
import os
import multiprocessing
import argparse
import subprocess

# Change to the pyhkfridge directory if needed
dn = os.path.dirname(__file__)
if dn:
	os.chdir(dn)

from pyhkfridgelib.fridge_script_controller import FridgeScriptController
from pyhkfridgeremote.control import pyhkfridge_get_active_ports

service_name = 'pyhkfridge.service'
service_fname = '/lib/systemd/system/' + service_name
service_text = '''
[Unit]
Description=pyhkfridge
After=multi-user.target

[Service]
User=%s
Group=%s
Type=idle
ExecStart=/usr/bin/python3 %s -n %i

[Install]
WantedBy=multi-user.target
'''

def run_controller(port):
	
	# Remove the logging handlers so we can move to per-thread files
	while len(logging.root.handlers) > 0:
		logging.root.removeHandler(logging.root.handlers[0])

	# Configure logging
	log_filename = os.path.join(APP_LOG_BASE_FOLDER, APP_LOG_FILENAME_PREFIX + str(port) + APP_LOG_FILENAME_SUFFIX)
	logging.basicConfig(filename=log_filename, format=APP_LOG_FORMAT, level=logging.DEBUG)
	screen_handler = logging.StreamHandler() 
	screen_handler.setFormatter(logging.Formatter(str(port) + " - " + APP_LOG_FORMAT))
	logging.getLogger().addHandler(screen_handler)  # Print to screen as well
	logging.info("Starting PyHK Fridge Controller " + VERSION_STR)

	fsc = FridgeScriptController(port)

	try:
		fsc.main_loop()
	except KeyboardInterrupt:
		pass
		
	logging.info("Exiting")

if __name__ == '__main__':
	
	setproctitle.setproctitle(PYHKFRIDGE_PROCNAME)
	
	# Command line arguments
	parser = argparse.ArgumentParser(
		formatter_class=argparse.RawDescriptionHelpFormatter,
		description='PyHK fridge script controller.', 
		epilog='Example:  ./pyhkfridge.py -n 3\n')
	parser.add_argument('-n', type=int, required=True, choices=list(range(1,9)), help='The number of instances to run.  Multiple instances allow multiple fridge scripts to be run or scheduled simultaneously.')
	parser.add_argument('--install', action='store_true', help='Install pyhkfridge (with the provided n value) as a systemd service that starts automatically at boot (run as root).')
	
	# They really should pass at least one argument.  If not, show the help.
	if len(sys.argv) <= 1: 
		parser.print_help()
		exit()
		
	args = parser.parse_args()
	n_inst = args.n
	
	assert(n_inst >= 1 and n_inst < 10)
	
	# Install as a systemd service
	if args.install:
		
		logging.info('Installing pyhkfridge systemd service (run as root)...')
				
		if os.path.exists(service_fname):
			os.remove(service_fname)
		
		user = os.environ["SUDO_USER"]
		with open(service_fname, "wt") as f:
			f.write(service_text % (user, user, os.path.abspath(__file__), n_inst))
		
		os.chmod(service_fname, 0o644)
		subprocess.call(['systemctl daemon-reload && systemctl enable ' + service_name + ' && systemctl restart ' + service_name], shell=True)
		
		logging.info('Installation complete.  Check the log feed with pyhk/tools/log_pyhkfridge.  Note that you may need to give yourself journal permissions and logout:\n    sudo usermod -a -G systemd-journal USERNAME')
		
		exit()
	
	# Choose an unused port
	active_ports = pyhkfridge_get_active_ports()
	p = PYHKFRIDGE_BASE_PORT
	ports = []
	while len(ports) < n_inst:
		if p > PYHKFRIDGE_MAX_PORT:
			logging.error("Not enough free pyhkfridge ports are available.  Too many instances of pyhkfridge are running, or another program is using the same ports.")
			exit()
		elif p not in active_ports:
			ports.append(p)
		p += 1
	
	logging.info("There are active instances on the following ports: " + str(active_ports))
	logging.info("Starting pyhkfridge with ports: " + str(ports))
	
	# Create processes if we need more than one instance
	proclist = []
	for i in range(len(ports)-1):
		proclist.append(multiprocessing.Process(target=run_controller, args=(ports[i],)))

    # Begin each instance
	for p in proclist:
		p.start()
	run_controller(ports[-1])
	
	exit()
	

	
