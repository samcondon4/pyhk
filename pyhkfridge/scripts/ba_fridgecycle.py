# Shortkeck Fridge Cycle v0.1.3

# 2014-10-16 JRH - Initial code written based on Animesh/Zak/Jon cycle
# 2015-03-23 JRH - Runs mce_zero_bias on start

# --- Valid Names ---
# LS218 Temperatures: ['4K Coldhead', '4K ColdPlate', '50K Coldhead', '50K ColdPlate', '4K Filter', '50K Filter']
# LS370 Temperatures: ['He4 Pump', 'He3 Pump', 'He4 HX', 'He3 HX', 'He4 Cond', 'He3 Cond', 'IC', 'UC', 'FPU #1', 'FPU #2', 'Spittoon']
# Voltages: ['He4_P', 'He4 HS', 'IC P', 'IC HS', 'UC P', 'UC HS']



import subprocess
from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):

	def get_steps(self):
		return [self.Step_0,
				self.Step_1,
				self.Step_2,
				self.Step_3,
				self.Step_4,
				self.Step_5]

	
	# Initialize everything
	def Step_0(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.set_voltage('4He P',  0.0)
		self.set_voltage('4He HS', 0.0)
		self.set_voltage('3He P',   0.0)
		self.set_voltage('3He HS',  0.0)
		
		self.show_status('All voltages set to zero.')
		
		#self.show_status('Attempting to run mce_zero_bias on keck97...')

		#subprocess.call(['ssh bicep@keck97.caltech.edu mce_zero_bias &'], shell=True)

		self.show_status('Beginning fridge cycle!')
		
		self.move_to_next_step()
	
	# Wait for the heat switches to cool, then turn on the He4 Pump
	def Step_1(self):
		
		self.show_status('Waiting for He4 and He3 heatswitches to cool to 7K...', add_to_log = False)
		
		if (self.get_temperature('4He HS') < 7) and (self.get_temperature('3He HS') < 7):
			
			self.show_status('He4 and He3 heatswitches to have cooled to 7K.')
			
			self.set_voltage('4He P',  25.0)
			self.show_status('Set He4 P to 25V.')
			
			self.move_to_next_step()
			
	# Wait for the He4 Pump to warm up, then turn it off and turn on the He3 pump
	def Step_2(self):
		
		self.show_status('Waiting for He4 P to warm to 60K...', add_to_log = False)
		
		if self.get_temperature('4He Pump') > 60:
			
			self.show_status('He4 P has warmed to 60K.')
			
			self.set_voltage('4He P',  0.0)
			self.show_status('Turned off He4 P voltage.')
			
			self.set_voltage('3He P',  25.0)
			self.show_status('Set He3 P to 25V.')
			
			self.move_to_next_step()
			

	# Wait for the He3 pump to warm up, then turn off the He3 pump
	def Step_3(self):
		
		self.show_status('Waiting for He3 p to warm to 60K...', add_to_log = False)
		
		if self.get_temperature('3He Pump') > 60:
			
			self.show_status('He3 p has warmed to 60K.')
			
			self.set_voltage('3He P',  0.0)
			self.show_status('Turned off He3 P voltage.')
			
			self.move_to_next_step()
	
	# Wait for the He3 condensation point (He4 Evap) to reach 4.5K, then turn on the He4 heatswitches
	def Step_4(self):
		
		self.show_status('Waiting for He3 condensation point (He4 Evap) to cool to 4.5 K...', add_to_log = False)
		
		if self.get_temperature('3He Cond') < 4.5:
			
			self.show_status('He3 condensation point (He4 Evap) has cooled to 4.5 K.')
			
			self.set_voltage('4He HS', 3.0)
			self.show_status('He4 heat switch set to 2V.')
			
			self.move_to_next_step()
			
	# Wait for the IC Evap to reach 2.2K, then turn on the He3 heatswitch
	def Step_5(self):
		
		self.show_status('Waiting for IC Evaporator to cool to 2.2 K...', add_to_log = False)
		
		if self.get_temperature('IC') < 2.2:
			
			self.show_status('IC Evaporator has cooled to 2.2 K.')
			
			self.set_voltage('3He HS',  3.0)
			self.show_status('Set He3 heatswitch to 3V. Good luck!')
			
			self.move_to_next_step()
