# K0 G measurement data colection

from __future__ import division, print_function

import subprocess,time
from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):

	def get_steps(self):
		self.time_last_step = 0
		self.initial_v = 0
		return [self.Step_init,
                self.Collect_data]


	def Step_init(self):

		self.set_voltage('GPIB 8 negative 25V max', 0.0)

		self.time_last_step = time.time()

		self.show_status('Initial voltage set to zero.')

		self.show_status('Beginning k0 meas!')

		self.move_to_next_step()

	def Collect_data(self):
		
		if ((time.time() - self.time_last_step) > (60*45)) or self.initial_v == 0:

			
			self.initial_v-=0.1
			self.show_status('Increment voltage to %.2f'%(self.initial_v), add_to_log = True)
			self.time_last_step = time.time()
			self.set_voltage('GPIB 8 negative 25V max', self.initial_v)

		if self.initial_v < -2:
			self.move_to_next_step()
