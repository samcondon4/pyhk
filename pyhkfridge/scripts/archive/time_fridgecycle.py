# TIME Fridge Cycle v0.3

# 2015-02-23 JRH - Initial code
# 2015-03-04 JRH - Added config file
# 2016-01-25 JRH - Added UC support



import time
import os

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	CONFIG_FILE = 'time_fridgecycle.conf'

	STEP_START_IC = 1
	STEP_END_IC = 13
	STEP_START_UC = 14
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step0,
				self.step1,
				self.step2,
				self.step3,
				self.step4,
				self.step5,
				self.step6,
				self.step7,
				self.step8,
				self.step9,
				self.step10,
				self.step11,
				self.step12,
				self.step13,
				self.step14,
				self.step15,
				self.step16,
				self.step17,
				self.step18]
	
	#########  Cycle Init  #########  
	
	# Initialize the cycle
	def step0(self):
		self.show_status('Initializing fridge cycle...')
		self.current_fridge = self.get_config('IC_START_FRIDGE')
		self.cycle_iteration = 0
		self.move_to_next_step()
	
	#########  IC Cycle Steps  ######### 
	
	# Initialize this particular iteration
	def step1(self):
		self.show_status('Cycle iteration ' + str(self.cycle_iteration) + ' is starting with ' + self.current_fridge)
		self.use_config_section(self.current_fridge)
		
		# Check if we are enabled
		if not self.get_config('ENABLED'):
			self.show_status(self.current_fridge + ' cycle is disabled, skipping.')
			self.move_to_step(self.STEP_END_IC)
		else:		
			self.disable_fridge_heaters()
			self.move_to_next_step()
		
	# Wait for the Evap-mK and Pump-4K switches to cool
	def step2(self):
		
		HS_PUMP_4K = self.get_config('NAME_HS_PUMP_4K')
		HS_EVAP_mK = self.get_config('NAME_HS_EVAP_mK')
		T_COOL_TARGET_HS = self.get_config('T_COOL_TARGET_HS')
		
		self.show_status('Waiting for the Evap-mK and Pump-4K heatswitches to cool to ' + str(T_COOL_TARGET_HS) + ' K...', add_to_log = False)
		
		if (self.get_temperature(HS_PUMP_4K) < T_COOL_TARGET_HS) and (self.get_temperature(HS_EVAP_mK) < T_COOL_TARGET_HS):
			self.show_status('Evap-mK and Pump-4K heatswitches cooled to to ' + str(T_COOL_TARGET_HS) + ' K.')
			self.move_to_next_step()
					
	# Heat up the pump and connect Evap to 1K
	def step3(self):	
		
		HS_EVAP_1K_HTR = self.get_config('NAME_HS_EVAP_1K_HTR')
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		P_HEAT_PUMP_HTR = self.get_config('P_HEAT_PUMP_HTR')
		P_HEAT_HS_EVAP_1K = self.get_config('P_HEAT_HS_EVAP_1K')
		
		self.set_power(HS_EVAP_1K_HTR,  P_HEAT_HS_EVAP_1K)
		self.show_status('Heating up the Evap to 1K heat switch.')
		
		self.set_power(PUMP_HTR, P_HEAT_PUMP_HTR)
		self.show_status('Putting ' + str(P_HEAT_PUMP_HTR*1000) + ' mW on the pump heater.')
		
		self.move_to_next_step()
			
	# Wait for the pump to warm up, then turn it off
	@fail_on_timeout(timeout_hours=3.0)
	def step4(self):
		
		PUMP = self.get_config('NAME_PUMP')
		T_WARM_TARGET_PUMP = self.get_config('T_WARM_TARGET_PUMP')
		
		self.show_status('Waiting for the pump to warm to ' + str(T_WARM_TARGET_PUMP) +' K...', add_to_log = False)
		
		if self.get_temperature(PUMP) > T_WARM_TARGET_PUMP:		
			self.show_status('Pump has warmed to ' + str(T_WARM_TARGET_PUMP) + ' K.')
			self.move_to_next_step()
			
	# Turn off the pump
	def step5(self):
					
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		
		self.output_off(PUMP_HTR)
		self.show_status('Pump heater off.')
		
		self.move_to_next_step()

	# Wait for Evap to cool down
	@skip_on_timeout(timeout_hours=3.0)
	def step6(self):
		
		T_COOL_TARGET_EVAP = self.get_config('T_COOL_TARGET_EVAP_CONDENSE')
		
		self.show_status('Waiting for Evap to cool to ' + str(T_COOL_TARGET_EVAP) + ' K...', add_to_log = False)
		
		EVAP = self.get_config('NAME_EVAP')
		
		if self.get_temperature(EVAP) < T_COOL_TARGET_EVAP:
			self.show_status('Evap has cooled to ' + str(T_COOL_TARGET_EVAP) + ' K')
			self.move_to_next_step()
			
	# Disengage the Evap-1K heat switch 
	def step7(self):
			
		HS_EVAP_1K_HTR = self.get_config('NAME_HS_EVAP_1K_HTR')
		
		self.output_off(HS_EVAP_1K_HTR)
		self.show_status('Evap-1K HS heater turned off.')
		
		self.move_to_next_step()
	
	# Wait for the Evap-1K HS to cool, then start pumping
	def step8(self):
		
		T_COOL_TARGET_HS = self.get_config('T_COOL_TARGET_HS')
		HS_EVAP_1K = self.get_config('NAME_HS_EVAP_1K')
		
		self.show_status('Waiting for the Evap-1K HS to cool to ' + str(T_COOL_TARGET_HS) + ' K...', add_to_log = False)
		
		if self.get_temperature(HS_EVAP_1K) < T_COOL_TARGET_HS:
			self.show_status('Evap-1K HS cooled to ' + str(T_COOL_TARGET_HS) + ' K.')
			self.move_to_next_step()
			
	# Start pumping
	def step9(self):

		P_HEAT_HS_PUMP_4K = self.get_config('P_HEAT_HS_PUMP_4K')
		HS_PUMP_4K_HTR = self.get_config('NAME_HS_PUMP_4K_HTR')
		
		self.set_power(HS_PUMP_4K_HTR, P_HEAT_HS_PUMP_4K)
		self.show_status('Heating up the Pump to 4K heat switch.')
		
		self.move_to_next_step()
			
	# Wait for Evap to cool
	def step10(self):
		
		EVAP = self.get_config('NAME_EVAP')
		T_COOL_TARGET_EVAP = self.get_config('T_COOL_TARGET_EVAP_FINAL')
		
		self.show_status('Waiting for Evap to cool to ' + str(T_COOL_TARGET_EVAP) + ' K...', add_to_log = False)
		
		if self.get_temperature(EVAP) < T_COOL_TARGET_EVAP:
			self.show_status('Evap has cooled to ' + str(T_COOL_TARGET_EVAP) + ' K.')			
			self.move_to_next_step()
			
	# Connect Evap to the mK stage
	def step11(self):
			
		HS_EVAP_mK_HTR = self.get_config('NAME_HS_EVAP_mK_HTR')
		P_HEAT_HS_EVAP_mK = self.get_config('P_HEAT_HS_EVAP_mK')
		
		self.set_power(HS_EVAP_mK_HTR,  P_HEAT_HS_EVAP_mK)
		self.show_status('Heating up the Evap to 300 mK heat switch.')
		
		self.wait_time_sec_start = time.time()
		self.cycle_iteration += 1
		
		self.move_to_next_step()
		
	# Wait to start the next cycle
	def step12(self):
		
		wait_time_sec = self.get_config('WAIT_BETWEEN_CYCLES_HR') * 3600
		time_remaining_sec = (wait_time_sec - (time.time() - self.wait_time_sec_start))
		
		if time_remaining_sec <= 0:
			self.move_to_next_step()
		else:
			self.show_status('Waiting to start next cycle (' + str(round(time_remaining_sec/3600,2)) + ' hr remaining)', add_to_log=False)
	
	# Begin the next iteration
	def step13(self):
		
		# Re-run the IC if needed
		if self.current_fridge == self.get_config('IC_START_FRIDGE'):
			if self.current_fridge == 'IC 1':
				self.current_fridge = 'IC 2'
			else:
				self.current_fridge = 'IC 1'
		
			# Go back to the beginning, just after initializing the cycle
			# and just before initializing the iteration
			self.show_status('Beginning next IC cycle iteration...')
			self.move_to_step(self.STEP_START_IC)
		
		else:
			self.move_to_next_step()
	
	#########  UC Cycle Steps  ######### 
	
	# Initialize the UC
	def step14(self):
		self.show_status('Beginning UC cycle...')
		self.current_fridge = 'UC'
		self.use_config_section(self.current_fridge)
		
		# Check if we are enabled
		if not self.get_config('ENABLED'):
			self.show_status(self.current_fridge + ' cycle is disabled, skipping.')
			self.complete_script()
		
		self.disable_fridge_heaters()
		self.move_to_next_step()
	
	# Wait for the Pump-4K switches to cool
	def step15(self):
		
		HS_PUMP_4K = self.get_config('NAME_HS_PUMP_4K')
		T_COOL_TARGET_HS = self.get_config('T_COOL_TARGET_HS')
		
		self.show_status('Waiting for the UC Pump-4K heatswitch to cool to ' + str(T_COOL_TARGET_HS) + ' K...', add_to_log = False)
		
		if (self.get_temperature(HS_PUMP_4K) < T_COOL_TARGET_HS):
			self.show_status('UC Pump-4K heatswitch cooled to to ' + str(T_COOL_TARGET_HS) + ' K.')
			self.move_to_next_step()
					
	# Heat up the pump
	def step16(self):	
		
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		P_HEAT_PUMP_HTR = self.get_config('P_HEAT_PUMP_HTR')
		
		self.set_power(PUMP_HTR, P_HEAT_PUMP_HTR)
		self.show_status('Putting ' + str(P_HEAT_PUMP_HTR*1000) + ' mW on the UC pump heater.')
		
		self.move_to_next_step()
			
	# Wait for the pump to warm up, then turn it off
	@fail_on_timeout(timeout_hours=6.0)
	def step17(self):
		
		PUMP = self.get_config('NAME_PUMP')
		T_WARM_TARGET_PUMP = self.get_config('T_WARM_TARGET_PUMP')
		
		self.show_status('Waiting for the UC pump to warm to ' + str(T_WARM_TARGET_PUMP) +' K...', add_to_log = False)
		
		if self.get_temperature(PUMP) > T_WARM_TARGET_PUMP:		
			self.show_status('Pump has warmed to ' + str(T_WARM_TARGET_PUMP) + ' K.')
			self.move_to_next_step()
			
	# Turn off the pump heater, cool it down
	def step18(self):
					
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		
		self.output_off(PUMP_HTR)
		self.show_status('UC pump heater off.')
		
		P_HEAT_HS_PUMP_4K = self.get_config('P_HEAT_HS_PUMP_4K')
		HS_PUMP_4K_HTR = self.get_config('NAME_HS_PUMP_4K_HTR')
		
		self.set_power(HS_PUMP_4K_HTR, P_HEAT_HS_PUMP_4K)
		self.show_status('Heating up the Pump to 4K heat switch.')
		
		self.move_to_next_step()
	
			
	#########  Event Hooks  ######### 
			
	# A function that is called when the fridge cycle fails.  Use it to set things to a safe state.
	def failure_cleanup(self):
		self.disable_fridge_heaters()
		
	#########  Local Helper Functions  ######### 
		
	def disable_fridge_heaters(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.output_off('Evap 1 Heater')
		self.output_off('Evap 2 Heater')
		self.check_and_disable(self.get_config('NAME_HS_PUMP_4K_HTR'))
		self.check_and_disable(self.get_config('NAME_HS_EVAP_1K_HTR'))
		self.check_and_disable(self.get_config('NAME_HS_EVAP_mK_HTR'))
		self.check_and_disable(self.get_config('NAME_PUMP_HTR'))
		self.check_and_disable(self.get_config('NAME_EVAP_HTR'))
		
		self.show_status('All voltages outputs for this fridge have been set to 0.')
		
	def check_and_disable(self, name):
		if name != None:
			self.output_off(name)
		
		
