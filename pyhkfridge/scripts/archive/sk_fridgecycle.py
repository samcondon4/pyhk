# Shortkeck Fridge Cycle v0.1.3

# 2014-10-16 JRH - Initial code written based on Animesh/Zak/Jon cycle
# 2015-03-23 JRH - Runs mce_zero_bias on start

# --- Valid Names ---
# LS218 Temperatures: ['4K Coldhead', '4K ColdPlate', '50K Coldhead', '50K ColdPlate', '4K Filter', '50K Filter']
# LS370 Temperatures: ['4He Pump', '3He Pump', '4He HX', '3He HX', '4He Cond', '3He Cond', 'IC', 'UC', 'FPU #1', 'FPU #2', 'Spittoon']
# Voltages: ['4He_P', '4He HS', 'IC P', 'IC HS', 'UC P', 'UC HS']



import subprocess
from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		return [self.step_init, 
				self.step_wait_hs, 
				self.step_heat_4he, 
				self.step_heat_3he,
				self.step_cool_4he,
				self.step_cool_3he]
	
	# Initialize everything
	def step_init(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.set_voltage('4He P',  0.0)
		self.set_voltage('4He HS', 0.0)
		self.set_voltage('IC P',   0.0)
		self.set_voltage('IC HS',  0.0)
		self.set_voltage('UC P',   0.0)
		self.set_voltage('UC HS',  0.0)
		
		self.show_status('All voltages set to zero.')
		
		self.show_status('Attempting to run mce_zero_bias on keck97...')

		subprocess.call(['ssh bicep@keck97.caltech.edu mce_zero_bias &'], shell=True)

		self.show_status('Beginning fridge cycle!')
		
		self.move_to_next_step()
	
	# Wait for the heat switches to cool, then turn on the 4He pump
	def step_wait_hs(self):
		
		self.show_status('Waiting for 4He and 3He heatswitches to cool to 7K...', add_to_log = False)
		
		if (self.get_temperature('4He HX') < 7) and (self.get_temperature('3He HX') < 7):
			
			self.show_status('4He and 3He heatswitches to have cooled to 7K.')
			
			self.set_voltage('4He P',  25.0)
			self.show_status('Set 4He pump to 25V.')
			
			self.move_to_next_step()
			
	# Wait for the 4He pump to warm up, then turn it off and turn on the 3He pumps
	def step_heat_4he(self):
		
		self.show_status('Waiting for 4He pump to warm to 60K...', add_to_log = False)
		
		if self.get_temperature('4He Pump') > 60:
			
			self.show_status('4He pump has warmed to 60K.')
			
			self.set_voltage('4He P',  0.0)
			self.show_status('Turned off 4He pump voltage.')
			
			self.set_voltage('IC P',  25.0)
			self.set_voltage('UC P',  10.0)
			self.show_status('Set IC and UC pumps to 25V and 10V respectively.')
			
			self.move_to_next_step()
			

	# Wait for the 3He pumps to warm up, then turn off the 3He pumps
	def step_heat_3he(self):
		
		self.show_status('Waiting for 3He pump to warm to 60K...', add_to_log = False)
		
		if self.get_temperature('3He Pump') > 60:
			
			self.show_status('3He pump has warmed to 60K.')
			
			self.set_voltage('IC P',  0.0)
			self.set_voltage('UC P',  0.0)
			self.show_status('Turned off IC and UC pump voltages.')
			
			self.move_to_next_step()
	
	# Wait for the 3He condensation point to reach 4.5K, then turn on the 4He heatswitches
	def step_cool_4he(self):
		
		self.show_status('Waiting for 3He condensation point to cool to 4.5 K...', add_to_log = False)
		
		if self.get_temperature('3He Cond') < 4.5:
			
			self.show_status('3He condensation point has cooled to 4.5 K.')
			
			self.set_voltage('4He HS', 2.0)
			self.show_status('4He heat switch set to 2V.')
			
			self.move_to_next_step()
			
	# Wait for the 3He condensation point to reach 2.2K, then turn on the 3He heatswitches
	def step_cool_3he(self):
		
		self.show_status('Waiting for 3He condensation point to cool to 2.2 K...', add_to_log = False)
		
		if self.get_temperature('3He Cond') < 2.2:
			
			self.show_status('3He condensation point has cooled to 2.2 K.')
			
			self.set_voltage('IC HS',  3.0)
			self.set_voltage('UC HS',  1.5)
			self.show_status('Set IC and UC heatswitches to 3V and 1.5V respectively.')
			
			self.move_to_next_step()

