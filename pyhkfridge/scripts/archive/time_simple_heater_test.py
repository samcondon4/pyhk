# TIME Heater Power Test v1.1

# 2015-02-25 JRH - Initial code
# 2015-03-05 JRH - Switched to using power instead of voltage
# 2017-03-24 ATC - Made this a lot simpler since we don't need to save the data seperately



import datetime
import numpy as np
import time

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	#~ ### 300 mK ###
	#~ HEATER_NAMES = ['300mK Heater']
	#~ THERMOMETER_NAMES = ['300mK']
	#~ AUX_THERMOMETER_NAMES = ['Evap 1', 'Evap 2', '1K Pot', '1K Strap', '1K Shield']
	#~ MANUAL_POWERS = False
	#~ MAX_POWER = 0.150e-3 # W
	#~ SETTLE_TIME = 40*60 # Seconds
	#~ NUM_POWER_STEPS = 7
	
	#~ ### Evap Solo ###
	#HEATER_NAMES = ['Evap 1 Heater', 'Evap 2 Heater']
	#THERMOMETER_NAMES = ['Evap 1', 'Evap 2']
	#AUX_THERMOMETER_NAMES = ['300mK', 'Evap 1', 'Evap 2', '1K Pot', '1K Strap', '1K Shield']
	#MANUAL_POWERS = True
	#POWERS = [0e-3, 0.05e-3, 0.100e-3, 0.125e-3, 0.150e-3]
	#SETTLE_TIME = 30*60 # Seconds
	

	#~ ### UC ###
	#HEATER_NAMES = ['Fridge UC Heater']
	#THERMOMETER_NAMES = ['Fridge UC']
	#AUX_THERMOMETER_NAMES = ['Fridge UC', '250mK Box Shelf 1', '250mK Box Shelf 2', '350mK Box IC Strap', 'Evap 1', 'Evap 2', '1K Pot', '1K Plate', '4K Plate', '4K Strap', '4K Head']
	#MANUAL_POWERS = True
	#POWERS = [0.001e-3, 0.003e-3, 0.005e-3, 0.01e-3, 0.015e-3, 0.025e-3, 0.04e-3, 0.05e-3, 0.00e-3]
	#SETTLE_TIME = 30*60 # Seconds
	
	#~ ### IC 1 ###
	HEATER_NAMES = ['Evap 1 Heater']
	THERMOMETER_NAMES = ['Evap 1']
	#AUX_THERMOMETER_NAMES = ['Fridge UC', '250mK Box Shelf 1', '250mK Box Shelf 2', '350mK Box IC Strap', 'Evap 1', 'Evap 2', '1K Pot', '1K Plate', '4K Plate', '4K Strap', '4K Head']
	MANUAL_POWERS = True
#	POWERS = [0.003e-3, 0.01e-3, 0.05e-3, 0.150e-3, 0.3e-3, 0.0]#, 0.5e-3, 1.0e-3]
	POWERS = [2e-3,0.0]
	SETTLE_TIME = 60*60*4 # Seconds
        #SETTLE_TIME = 10 # Seconds
	
	### Heat Switches ###
	#~ HEATER_NAMES = ['HS ' + str(i) + ' Heater' for i in range(1,7)]
	#~ THERMOMETER_NAMES = ['HS ' + str(i) for i in range(1,7)]
	#~ AUX_THERMOMETER_NAMES = []
	#~ MAX_POWER = 1.5e-3 # W
	#~ MANUAL_POWERS = False
	#~ SETTLE_TIME = 30*60 # Seconds
	#~ NUM_POWER_STEPS = 5
	
	SAVEFILE_NAME = 'heater_test_result_20170330_Evap2_boxheater_wait30.txt'
	#SAVEFILE_NAME = 'heater_test_result_20170324_UC_Evap1_wait30.txt'
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step0,
				self.step1,
				self.step2,
				self.step3,
				self.step4,
				self.step5]
	
	# Initialize this run
	def step0(self):
		
		self.show_status('Starting ' + str(len(self.HEATER_NAMES)) + ' heater tests.')
		
		self.cur_heater_index = 0
		self.cur_heater_name = self.HEATER_NAMES[self.cur_heater_index]
		self.cur_thermometer_name = self.THERMOMETER_NAMES[self.cur_heater_index]
		
		self.move_to_next_step()
		
	# Initialize this run
	def step1(self):
		self.show_status('Starting heater test with ' + self.cur_heater_name + ' and ' + self.cur_thermometer_name)
		
		# Start in a known state
		self.output_off(self.cur_heater_name)
		
			
		# Linearly sample power space
		if self.MANUAL_POWERS:
			self.powers_to_use = self.POWERS
			self.NUM_POWER_STEPS = len(self.POWERS)
		else:
			self.powers_to_use = np.round(np.linspace(0,self.MAX_POWER,self.NUM_POWER_STEPS),3).tolist()
		self.current_power_index = 0
		
		self.show_status('Planning to try the following powers in W: ' + str(self.powers_to_use))
		
		self.move_to_next_step()

	# Set the power
	def step2(self):
		
		new_power =  self.powers_to_use[self.current_power_index]
		self.show_status('Setting Power: ' + str(new_power))
		self.set_power(self.cur_heater_name, new_power)
		self.last_power_update = time.time()
		
		self.move_to_next_step()
	
	# Wait for it to settle
	def step3(self):
		
		time_until_settled = (self.SETTLE_TIME - (time.time() - self.last_power_update))
		
		if time_until_settled <= 0:
			self.show_status('Temperature is now assumed to have settled')
			self.move_to_next_step()
		else:
			self.show_status('Waiting for temperature to settle (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)
			
	# Save the values and decide how to proceed
	def step4(self):
		
		power_applied = round(self.powers_to_use[self.current_power_index],3)
		voltage_measured = round(self.get_voltage(self.cur_heater_name),3)
		power_measured = round(self.get_power(self.cur_heater_name),3)
		temperature_measured = round(self.get_temperature(self.cur_thermometer_name),3)
		
		self.show_status('Results for ' + str(power_applied) + ' W applied: ' + str(voltage_measured) + ' V, ' + str(power_measured) + ' W, ' + str(temperature_measured) + ' K')

				
		self.current_power_index += 1
		if self.current_power_index >= self.NUM_POWER_STEPS:
			self.move_to_next_step()
		else:
			self.move_to_step(2)
	
		
	# Loop through the heaters
	def step5(self):
		
		self.cur_heater_index += 1
		if self.cur_heater_index >= len(self.HEATER_NAMES):
			self.move_to_next_step()
		else:
			self.cur_heater_name = self.HEATER_NAMES[self.cur_heater_index]
			self.cur_thermometer_name = self.THERMOMETER_NAMES[self.cur_heater_index]
			self.move_to_step(1)
		

