# TIME PT head load curve

import datetime
import numpy as np
import time
import subprocess

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	#~ HEATER_NAME = "4K Plate Heater"
	#~ SETTLE_TIME = 30*60 # Seconds (time to wait after changing a power)
	#~ MAX_VOLTAGE = 25 # V
	#~ #MIN_VOLTAGE = 0 # V
	#~ #NUM_VOLTAGE_STEPS = 10
	#~ MIN_VOLTAGE = 11.1 # V
	#~ NUM_VOLTAGE_STEPS = 7
	
	# ~ HEATER_NAME = "50K Plate Heater"
	# ~ MIN_VOLTAGE = 0 # V
	# ~ MAX_VOLTAGE = 20 # V
	# ~ SETTLE_TIME = 45*60 # Seconds (time to wait after changing a power)
	# ~ NUM_VOLTAGE_STEPS = 12
	
	HEATER_NAME = "1K Pot Heater"
	MIN_VOLTAGE = 0 # V
	MAX_VOLTAGE = 4 # V
	SETTLE_TIME = 45*60 # Seconds (time to wait after changing a power)
	NUM_VOLTAGE_STEPS = 6

	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step0,
				self.step1,
				self.step2,
				self.step3,
				self.step4]
				
	# Initialize this run
	def step0(self):

		self.show_status('Starting measurment')
		
		# Start in a known state
		self.output_off(self.HEATER_NAME)
		
		# Linearly sample power space
		self.voltages_to_use = np.sqrt(np.linspace(self.MIN_VOLTAGE**2,self.MAX_VOLTAGE**2,self.NUM_VOLTAGE_STEPS))
		self.voltages_to_use = np.round(self.voltages_to_use, 1)
		self.voltages_to_use = self.voltages_to_use.tolist()
		
		self.current_voltage_index = 0
		
		self.show_status('Planning to try the following voltages: ' + str(self.voltages_to_use))
		
		self.move_to_next_step()

	# Blast normal and set the new target power
	def step1(self):
		
		new_v = self.voltages_to_use[self.current_voltage_index]
		
		self.set_voltage(self.HEATER_NAME, new_v)
		self.last_voltage_update = time.time()
		
		self.show_status('Set heater voltage to: ' + str(new_v))
		
		self.move_to_next_step()
	
	# Pause to let re-enabled thermometers update
	@pause_cycle(duration_seconds = SETTLE_TIME)
	def step2(self, time_remaining="?"):
		self.show_status('Waiting to thermalize...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
	
	# Wait for the load curve to finish before changing the power
	def step3(self):

		# Decide if we are finished or not
		self.current_voltage_index += 1
		if self.current_voltage_index >= self.NUM_VOLTAGE_STEPS:
			self.move_to_next_step()
		else:
			self.move_to_step(1)
	
	# Write out any final state data
	def step4(self):
		
		self.show_status('Turning heater off.')
		self.output_off(self.HEATER_NAME)
		self.move_to_next_step()

	

