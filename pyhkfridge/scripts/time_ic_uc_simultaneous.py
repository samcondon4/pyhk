# TIME IC and UC together



import time
import os
import numpy as np

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		return [self.initCycle,
				self.waitForHS,
				self.heatPumps,
				self.waitPumpsHot,
				self.pauseCondense,
				self.startCool1KSwitch,
				self.waitCool1KSwitch,
				self.pumpIC,
				self.waitCoolUC,
				self.pumpUC,
				self.waitCoolICHS,
				self.pauseICHS,
				self.reheatIC,
				self.waitReheatIC,
				]
				
	
	def initCycle(self):
		
		self.show_status('Initializing')
		
		self.pumps_hot = [False, False, False]
		
		# ~ self.output_off('FPU Heater')
		# ~ self.output_off('FPU Heater #2')
		self.output_off('Evap 1 Heater')
		self.output_off('Evap 2 Heater')
		
		# Pump to 4K off
		self.output_off('HS 1 Heater')
		self.output_off('HS 4 Heater')
		self.output_off('HS 7 Heater')
		
		# IC 2 to box off
		self.output_off('HS 6 Heater')
		
		# IC 1 to box on
		self.set_power('HS 3 Heater',  1.5e-3)
		
		# Evap to 1K on
		#~ self.set_power('HS 2 Heater',  1.0e-3) # dead
		self.set_power('HS 5 Heater',  1.0e-3)
		
		self.move_to_next_step()

	# Wait for the three pump heat switches to cool
	def waitForHS(self):
		
		self.show_status('Waiting for pump heat switches to cool...', add_to_log = False)
		
		for hs in ['HS 1', 'HS 4', 'HS 7']:
			 if self.get_temperature(hs) > 6:
				 return
		
		self.show_status('Pump heat switches cooled')
		self.move_to_next_step()
		
		
	def heatPumps(self):
		
		self.show_status('Heating pumps')
		self.set_power('Pump 1 Heater',  120e-3)
		self.set_power('Pump 2 Heater',  120e-3)
		self.set_power('Pump 3 Heater',  120e-3)
		self.move_to_next_step()
		
	def waitPumpsHot(self):
		
		self.show_status('Waiting for pumps to heat...', add_to_log = False)
		
		pump_list = ['Pump 1', 'Pump 2', 'Pump 3']
		heat_maintain = [10e-3, 10e-3, 30e-3]
		T_pump_hot = [45, 45, 40]
		
		for i in range(len(pump_list)) :
			 if not self.pumps_hot[i] and self.get_temperature(pump_list[i]) > T_pump_hot[i]:
				 self.pumps_hot[i] = True
				 self.set_power(pump_list[i] + ' Heater', heat_maintain[i])
				 
		if np.all(self.pumps_hot):
			self.show_status('All pumps hot')
			self.move_to_next_step()
			
	@pause_cycle(duration_seconds = 1.5*60*60)
	def pauseCondense(self, time_remaining="?"):
		self.show_status('Waiting to condense...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
				
	def startCool1KSwitch(self):
		
		self.show_status('Cooling 1K-Evap heat switches')
		self.output_off('HS 2 Heater')
		self.output_off('HS 5 Heater')	
		self.move_to_next_step()
	
	def waitCool1KSwitch(self):
		
		self.show_status('Waiting for Evap-1K heat switches to cool...', add_to_log = False)
		if self.get_temperature('HS 2') < 6 and self.get_temperature('HS 5') < 6: 
			self.show_status('Evap-1K heat switches cooled')
			self.move_to_next_step()
			
	def pumpIC(self):	
		
		self.show_status('Pumping ICs')
		self.output_off('Pump 1 Heater')
		self.output_off('Pump 2 Heater')
		self.set_power('HS 1 Heater',  1.3e-3)
		self.set_power('HS 4 Heater',  1.0e-3)
		self.move_to_next_step()
	
	@skip_on_timeout(timeout_hours=1.0)
	def waitCoolUC(self):
		
		self.show_status('Waiting for UC to cool...', add_to_log = False)
		if self.get_temperature('Fridge UC') < 0.7:
			self.show_status('UC cooled')
			self.move_to_next_step()
		
	def pumpUC(self):
		
		self.show_status('Pumping UC')
		
		self.output_off('Pump 3 Heater')
		self.set_power('HS 7 Heater',  1.0e-3)
		
		# Switch ICs
		self.output_off('HS 3 Heater')
		self.set_power('HS 6 Heater',  1.5e-3)
		self.output_off('HS 1 Heater')

		self.move_to_next_step()
		
	def waitCoolICHS(self):
		
		self.show_status('Waiting for IC heat switches to cool...', add_to_log = False)
		
		for hs in ['HS 1', 'HS 3']:
			 if self.get_temperature(hs) > 9:
				 return
		
		self.show_status('IC heat switches cooled')
		self.move_to_next_step()
		
	@pause_cycle(duration_seconds = 1.0*60*60)
	def pauseICHS(self, time_remaining="?"):
		self.show_status('Waiting for HS3 to fully open...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
		
	def reheatIC(self):
		
		self.show_status('Heating pump 1 somewhat slowly')
		self.set_power('Pump 1 Heater',  40e-3)
		self.move_to_next_step()
		
	def waitReheatIC(self):
		
		self.show_status('Waiting for IC pump to heat...', add_to_log = False)

		if self.get_temperature('Pump 1') > 40:
			self.set_power('Pump 1 Heater', 10e-3)
			self.show_status('IC pump hot, maintaining')
			self.move_to_next_step()


