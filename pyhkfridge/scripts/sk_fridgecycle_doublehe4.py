# Shortkeck Fridge Cycle v0.1.3

# 2014-10-16 JRH - Initial code written based on Animesh/Zak/Jon cycle
# 2015-03-23 JRH - Runs mce_zero_bias on start
# 2015-12-01 BAS - Triple helium 4 cycle

# --- Valid Names ---
# LS218 Temperatures: ['4K Coldhead', '4K ColdPlate', '55K Coldhead', '55K ColdPlate', '4K Filter', '55K Filter']
# LS370 Temperatures: ['4He Pump', '3He Pump', '4He HX', '3He HX', '4He Cond', '3He Cond', 'IC', 'UC', 'FPU #1', 'FPU #2', 'Spittoon']
# Voltages: ['4He_P', '4He HS', 'IC P', 'IC HS', 'UC P', 'UC HS']



import subprocess
import os
import time

from pyhkfridgelib.fridge_script_base import *
from pyhkdremote.data_loader import pyhkd_get_config_dir

LS370_CONFIG_PATH = os.path.join(pyhkd_get_config_dir(), 'ls370_sk.json')

# True is enabled, False is disabled, None is don't change
LS370_THEMOMETER_ENABLED = [True, True, True, True,
							True, True, True, True,
							None, True, None, True,
							None, None, None, None]

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		# Note: this script can be refactored to re-use the 4He steps!
		return [self.step_init,
				self.step_init_pause,
				self.step_wait_hs_1,
				self.step_heat_4he_1,
				self.step_heat_3he,
				self.step_pause_warmpump,
				self.step_cool_4he_1,
				self.step_wait_4he_1,
				self.step_wait_hs_2,
				self.step_heat_4he_2,
				self.step_pause_warmpump,
				self.step_cool_4he_2,
				self.step_wait_4he_2,
				self.step_cool_3he]
	
	# Initialize everything
	def step_init(self):
		
		self.show_status('Enabling LS370 thermometers...')
		
		self.ls370_set_enabled(LS370_CONFIG_PATH, LS370_THEMOMETER_ENABLED)
		
		self.show_status('Attempting to run mce_zero_bias on keck97...')

		subprocess.call(['ssh bicep@keck97.caltech.edu mce_zero_bias &'], shell=True)
			
		self.show_status('Turning off all heat switches and pumps...')
		
		self.set_voltage('4He P',  0.0)
		self.set_voltage('4He HS', 0.0)
		self.set_voltage('IC P',   0.0)
		self.set_voltage('IC HS',  0.0)
		self.set_voltage('UC P',   0.0)
		self.set_voltage('UC HS',  0.0)
		
		self.show_status('All voltages set to zero.')
		
		self.show_status('Beginning fridge cycle!')
				
		self.move_to_next_step()
	
	# Pause 5 min to let re-enabled thermometers update
	@pause_cycle(duration_seconds = 5*60)
	def step_init_pause(self, time_remaining="?"):
		self.show_status('Waiting 5 min for any newly enabled thermometers to update...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
	
	# First He4 heat
	def step_wait_hs_1(self):
		self.show_status('Waiting for 4He and 3He heatswitches to cool to 7K...', add_to_log = False)
		if (self.get_temperature('4He HX') < 7) and (self.get_temperature('3He HX') < 7):
			self.show_status('4He and 3He heatswitches have cooled to 7K.')
			self.set_voltage('4He P',  25.0)
			self.show_status('First 4He heat - Set 4He pump to 25V.')
			self.move_to_next_step()
			
	def step_heat_4he_1(self):
		self.show_status('Waiting for 4He pump to warm to 55K...', add_to_log = False)
		if self.get_temperature('4He Pump') > 55:
			self.show_status('4He pump has warmed to 55K.')
			self.set_voltage('4He P',  7.0)
			self.show_status('Set 4He pump voltage to quiescent level.')
			self.set_voltage('IC P',  25.0)
			self.set_voltage('UC P',  10.0)
			self.show_status('Set IC and UC pumps to 25V and 10V respectively.')
			self.move_to_next_step()
			
	def step_heat_3he(self):
		self.show_status('Waiting for 3He pump to warm to 55K...',add_to_log=False)
		if self.get_temperature('3He Pump') > 55:
			self.show_status('3He pump has warmed to 55K.')
			self.set_voltage('IC P',  7.0)
			self.set_voltage('UC P',  6.0)
			self.show_status('Turned down IC and UC pump voltages to quiescent levels')
			self.move_to_next_step()
	
	@pause_cycle(duration_seconds = 20*60)
	def step_pause_warmpump(self, time_remaining="?"):
		self.show_status('Waiting 20 minutes with warm pump...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
		
	# First He4 cool
	def step_cool_4he_1(self):
		self.show_status('Turning off 4He Pump and turning on 4He HS')
		self.set_voltage('4He P',  0.0)
		self.set_voltage('4He HS', 2.0)
		self.move_to_next_step()
			
	def step_wait_4he_1(self):
		self.show_status('Waiting for pump to cool to 7K...',add_to_log=False)
		if self.get_temperature('4He Pump') < 7.0:
			self.show_status('4He Pump has cooled to 7K')
			self.set_voltage('4He HS',  0.0)
			self.move_to_next_step()

	# Wait for 4He HX to cool and second He4 heat
	def step_wait_hs_2(self):
		self.show_status('Waiting for 4He heatswitch to cool to 7K...',add_to_log=False)
		if (self.get_temperature('4He HX') < 7):
			self.show_status('4He heatswitch has cooled to 7K')
			self.show_status('Turning on 4He P - second 4He heat')
			self.set_voltage('4He P',  25.0)
			self.move_to_next_step()

	# Wait for He4 to heat up
	def step_heat_4he_2(self):
		self.show_status('Waiting for 4He pump to warm to 55K...',add_to_log=False)
		if self.get_temperature('4He Pump') > 55:
			self.show_status('4He pump has warmed to 55K, coasting pump.')
			self.set_voltage('4He P',  7.0)
			self.move_to_next_step()

	# Wait with warm pump, then turn on switch
	def step_cool_4he_2(self):
		self.show_status('Disabling pump and turning on heat switch')
		self.set_voltage('4He P',0.0)
		self.set_voltage('4He HS',2.0)
		self.move_to_next_step()

	# Wait for pump to cool
	def step_wait_4he_2(self):
		self.show_status('Waiting for pump to cool to 7K...',add_to_log=False)
		if self.get_temperature('4He Pump') < 7.0:
			self.show_status('4He Pump has cooled to 7K')
			self.move_to_next_step()

	# Finally cool He3 pumps
	def step_cool_3he(self):
		self.show_status('Turning off IC/UC Pump heaters and turning on IC/UC switches')
		self.set_voltage('IC P',0.0)
		self.set_voltage('UC P',0.0)
		self.set_voltage('IC HS',  3.0)
		self.set_voltage('UC HS',  1.5)
		self.move_to_next_step()

