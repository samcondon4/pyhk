

import datetime
import numpy as np
import time
import subprocess

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	HEATER_NAME = "FPU Heater"
	HEATER_VOLTAGE = 0.15

	def get_steps(self):
		return [self.step_heater_off,
				self.step_pause,
				self.step_heater_on,
				self.step_pause,
				self.step_heater_off]
				
	@pause_cycle(duration_seconds = 20*60)
	def step_pause(self, time_remaining=0):
		self.show_status('Waiting...  (%0.1f min remaining)' % (time_remaining/60), add_to_log=False)
	
	def step_heater_on(self):
		self.show_status("Turning heater on")
		self.set_voltage(self.HEATER_NAME, self.HEATER_VOLTAGE)		
		self.move_to_next_step()
	
	def step_heater_off(self):
		self.show_status("Turning heater off")
		self.set_voltage(self.HEATER_NAME, 0)		
		self.move_to_next_step()
	
