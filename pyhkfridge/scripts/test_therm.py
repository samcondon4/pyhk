
from __future__ import division, print_function

import datetime
import numpy as np
import time

from pyhkfridgelib.fridge_script_base import *


class FridgeScript(FridgeScriptBase):


	def get_steps(self):
		return [self.test]

	def test(self):

		for i in range(1,4):
			name = "FPU #%i" % i
			t = self.get_temperature(name)
			self.show_status("%s = %0.2f mK" % (name, t*1000))
		self.move_to_next_step()


	

