# TIME G Measurement v1.0

# 2015-06-05 JRH - Initial code
# 2016-02-08 JRH - Modified to use ivcurve.py
# 2017-11-08 JRH - Modified for TIME cryostat

import datetime
import numpy as np
import time
import subprocess

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	UNLATCH_FIRST = True # Set to False only if you are starting unlatched and settled already
	UNLATCH_HEATER_NAME = "FPU Heater #2"
	# ~ HEATER_NAME = "FPU Heater #2"
	HEATER_NAME = "Fridge UC Heater"
	THERMOMETER_NAME = "FPU #3 Carbon Array" # 3
	#~ AUX_THERMOMETER_NAMES = ["FPU #2","Fridge UC","Fridge IC"] # Other thermometers to save
	AUX_THERMOMETER_NAMES = ["FPU #1","FPU #2","FPU #3","Fridge UC","Fridge IC","FPU #3 Carbon Array"] # Other thermometers to save
	AUX_RESISTOR_NAMES = [] # Other resistances to save
	MAX_POWER = 120*1e-6 # W
	# ~ MAX_POWER = 80*1e-6 # W
	# ~ SETTLE_TIME = 1.5*60*60 # Seconds (time to wait after changing a power)
	SETTLE_TIME = 70*60 # Seconds (time to wait after changing a power)
	REMOTE_WAIT_TIME = 400 # Seconds (time to wait for remote commands to finish)
	NUM_POWER_STEPS = 8
	UNLATCH_CMD = 'bias_tess 30000 && sleep 20 && bias_tess 2000'
	IVCURVE_CMD = '/home/time/b3_analysis/load_curve/ivcurve.py --bias_start 2000 --bias_step -1 --bias_count 2001 --settle_time 30 --settle_bias 2000 --bias_pause 0.1 --bias_final 2000 --data_mode 10 -d '
	BLAST_POWER = 300*1e-6 # W
	BLAST_TIME = 30 # sec

	# NOTE: bias the TESs to the settle bias before starting or the first temperature will latch!
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step0,
				self.step1,
				self.step2,
				self.step3,
				self.step4,
				self.step5]
				
	# Initialize this run
	def step0(self):
		
		self.show_status('Starting G measurment, monitoring temperature ' + self.THERMOMETER_NAME)
		self.show_status('NOTE: Are the SQUIDs tuned?')
		
		timestamp = int(time.time())
		self.savefile_name = '/home/time/glog/time_g_' + str(timestamp) + '.txt'
		self.loadcurve_prefix = 'iv_g_' + str(timestamp) 
				
		with open(self.savefile_name, 'a') as savefile:
			savefile.write('--- TIME G Measurement ---\n\n')
			savefile.write('Thermometer: ' + str(self.THERMOMETER_NAME) + '\n')
			savefile.write('Heater: ' + str(self.HEATER_NAME) + '\n')
			savefile.write('Starting Time: ' + str(datetime.datetime.now()) + '\n')
			savefile.write('Settle Time (sec): ' + str(self.SETTLE_TIME) + '\n')
			savefile.write('\nTime [s]\Power Applied [uW]\Power Measured [uW]\tTemperature Measured [K]')
			for n in self.AUX_THERMOMETER_NAMES:
				savefile.write('\t' + str(n) + ' [K]')
			for n in self.AUX_RESISTOR_NAMES:
				savefile.write('\t' + str(n) + ' [Ohm]')
			savefile.write('\n')
		
		# Linearly sample power space
		self.powers_to_use = np.linspace(0,self.MAX_POWER,self.NUM_POWER_STEPS).tolist()
		
		self.current_power_index = 0
		
		self.show_status('Planning to try the following powers in W: ' + str(self.powers_to_use))
		
		if self.UNLATCH_FIRST:
			# Start in a known state
			self.output_off(self.HEATER_NAME)
			self.output_off(self.UNLATCH_HEATER_NAME)
		
			self.move_to_next_step()
		else:
			self.show_status('Skipping setup for first point')
			self.move_to_step(3)

	# Blast normal and set the new target power
	def step1(self):
		
		new_power =  self.powers_to_use[self.current_power_index]
		
		self.show_status('Blasting TES bias to unlatch...')
		subprocess.call(['ssh time@time-mce-0.caltech.edu "' + self.UNLATCH_CMD + '" &'], shell=True)

		self.show_status('Blasting unlatch heater with ' + str(self.BLAST_POWER*1e6) + ' uW for ' + str(self.BLAST_TIME) + ' sec to unlatch...')
		self.set_power(self.UNLATCH_HEATER_NAME, self.BLAST_POWER)
		time.sleep(self.BLAST_TIME)
		self.set_power(self.UNLATCH_HEATER_NAME, 0)
		self.show_status('Setting Power: ' + str(new_power))
		self.set_power(self.HEATER_NAME, new_power)
		self.last_power_update = time.time()
		
		self.local_settle_time = self.SETTLE_TIME
		
		# Give the low power points a little extra time, since it
		# tends to need it
		if self.current_power_index <= 1:
			self.local_settle_time *= 1.5
		
		self.move_to_next_step()
	
	# Wait for it to settle
	def step2(self):
		
		time_until_settled = (self.local_settle_time - (time.time() - self.last_power_update))
		
		if time_until_settled <= 0:
			self.show_status('Temperature is now assumed to have settled')
			self.move_to_next_step()
		else:
			self.show_status('Waiting for temperature to settle (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)
			
	# Save the temperatures, take load curve
	def step3(self):
		
		power_applied = round(self.powers_to_use[self.current_power_index]*1e6,3)
		power_measured = round(self.get_power(self.HEATER_NAME)*1e6,3)
		temperature_measured = round(self.get_temperature(self.THERMOMETER_NAME),4)
		
		self.show_status('Results for ' + str(power_applied) + ' uW applied: ' + str(power_measured) + ' uW, ' + str(temperature_measured) + ' K')

		with open(self.savefile_name, 'a') as savefile:
			savefile.write(str(time.time()) + '\t' + str(power_applied) + '\t' + str(power_measured) + '\t' + str(temperature_measured) )
			for n in self.AUX_THERMOMETER_NAMES:
				savefile.write('\t' + str(round(self.get_temperature(n),4)))
			for n in self.AUX_RESISTOR_NAMES:
				savefile.write('\t' + str(round(self.get_resistance(n),4)))
			savefile.write('\n')
			
		# Take the load curve on time-mce-0
		try:
			lc_filename = self.loadcurve_prefix + '_' + str(int(round(1000*temperature_measured))) + 'mk'
		except ValueError:
			lc_filename = self.loadcurve_prefix + '_step' + str(self.current_power_index)
		self.show_status('Attempting to begin load curve ' + lc_filename + ' on time-mce-0.')
		subprocess.call(['ssh time@time-mce-0.caltech.edu ' + self.IVCURVE_CMD + lc_filename + ' &'], shell=True)
		self.loadcurve_start_time = time.time()
		
		self.move_to_next_step()
	
	# Wait for the load curve to finish before changing the power
	def step4(self):
		
		time_until_settled = (self.REMOTE_WAIT_TIME - (time.time() - self.loadcurve_start_time))
		
		if time_until_settled > 0:
			self.show_status('Pausing to allow the load curve to finish (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)
		else:	
			self.show_status('Load curve is assumed to have finished.')
			
			# Decide if we are finished or not
			self.current_power_index += 1
			if self.current_power_index >= self.NUM_POWER_STEPS:
				self.move_to_next_step()
			else:
				self.move_to_step(1)
	
	# Write out any final state data
	def step5(self):
		
		self.show_status('Turning heater off.')
		self.output_off(self.HEATER_NAME)
		
		with open(self.savefile_name, 'a') as savefile:
			savefile.write('\n\n')
			savefile.write('Ending Time: ' + str(datetime.datetime.now()) + '\n')
			savefile.write('\n\n-------------------\n\n')
			
		self.show_status('Measurement complete!')
			
		self.move_to_next_step()
	

