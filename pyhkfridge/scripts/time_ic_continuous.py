# TIME Continuous IC Cycle v1.0

# 2017-01-16 JRH - Modified from main fridge cycle



import time
import os

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	CONFIG_FILE = 'time_fridgecycle.conf'

	STEP_START_IC = 1
	STEP_END_IC = 17
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.initializeIC,
			self.initializeIteration,
			self.waitForUC,
			self.coolSwitches,
			self.waitICSwitch,
			self.pumpOn,
			self.heatPump,
			self.pumpMaintain,
			self.condenseEvap,
			self.evap1KHeatSwitchOff,
			self.waitForEvapToCool,
			self.startPumping,
			self.pumpSlowly,
			self.pumpFully,
			self.coolingEvap,
			self.pauseSettle,
			self.connectMkStage,
			self.waitForNextCycle,
			self.startNextCycle]
	
	#########  Cycle Init  #########  
	
	# Initialize the cycle
	def initializeIC(self):
		self.show_status('Initializing infinite IC cycle...')
		self.current_fridge = self.get_config('IC_START_FRIDGE')
		self.cycle_iteration = 0
		self.move_to_next_step()
	
	#########  IC Cycle Steps  ######### 
	
	# Initialize this particular iteration
	def initializeIteration(self):
		self.show_status('Cycle iteration ' + str(self.cycle_iteration) + ' is starting with ' + self.current_fridge)
		self.use_config_section(self.current_fridge)
		
		# Check if we are enabled
		if not self.get_config('ENABLED'):
			self.show_status(self.current_fridge + ' cycle is disabled, skipping.')
			self.move_to_step(self.STEP_END_IC)
		else:
			self.disable_fridge_heaters()
			self.move_to_next_step()
			
	# Don't run the IC if the UC is mid cycle
	def waitForUC(self):
		
		self.move_to_next_step()
		
		# ~ self.show_status('Waiting for the UC pump HS to turn on and the UC pump to cool so the cycles do not interfere...', add_to_log = False)
		
		# ~ if self.get_voltage('HS 7 Heater') > 0.1 and self.get_temperature("Pump 3") < 10:		
			# ~ self.move_to_next_step()
	
	# Wait for the Evap-mK and Pump-4K switches to cool
	def coolSwitches(self):
		
		HS_PUMP_4K = self.get_config('NAME_HS_PUMP_4K')
		HS_EVAP_mK = self.get_config('NAME_HS_EVAP_mK')
		T_COOL_TARGET_HS_EVAP_mK = self.get_config('T_COOL_TARGET_HS_EVAP_mK')
		T_COOL_TARGET_HS_PUMP_4K = self.get_config('T_COOL_TARGET_HS_PUMP_4K')
		
		self.show_status('Waiting for the Evap-mK HS to cool to ' + str(T_COOL_TARGET_HS_EVAP_mK) + ' K and Pump-4K HS to cool to ' + str(T_COOL_TARGET_HS_PUMP_4K) + ' K...', add_to_log = False)
		
		if (self.get_temperature(HS_PUMP_4K) < T_COOL_TARGET_HS_PUMP_4K) and (self.get_temperature(HS_EVAP_mK) < T_COOL_TARGET_HS_EVAP_mK):
			self.show_status('Evap-mK and Pump-4K heatswitches cooled.')
			self.move_to_next_step()
	
	# Pause to make sure HS3 or HS6 turns off fully. See the
	# BLISS fridge report.  It looks like we need an hour to do this
	# properly, but I don't want to do that for the single shot cycle.
	# Perhaps for the continuous cycle.
	@pause_cycle(duration_seconds = 2.0*60*60)
	def waitICSwitch(self, time_remaining="?"):
		self.show_status('Waiting for heat switch to fully turn off...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
		
	# Heat up the pump and connect Evap to 1K
	def pumpOn(self):	
		
		HS_EVAP_1K_HTR = self.get_config('NAME_HS_EVAP_1K_HTR')
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		P_HEAT_PUMP_HTR = self.get_config('P_HEAT_PUMP_HTR')
		P_HEAT_HS_EVAP_1K = self.get_config('P_HEAT_HS_EVAP_1K')
		
		self.set_power(HS_EVAP_1K_HTR,  P_HEAT_HS_EVAP_1K)
		self.show_status('Heating up the Evap to 1K heat switch.')
		
		#~ self.set_power(PUMP_HTR, P_HEAT_PUMP_HTR)
		#~ self.show_status('Putting ' + str(P_HEAT_PUMP_HTR*1000) + ' mW on the pump heater.')
		
		# Set up to heat slowly
		self.pump_power_current = 0
		self.pump_power_target = P_HEAT_PUMP_HTR
		self.pump_power_time = time.time()
		self.pump_power_dt = 60
		self.pump_power_dp = 1.0e-3

		self.set_power(PUMP_HTR, self.pump_power_current)
		self.show_status('Heating up the pump slowly')
		
		self.move_to_next_step()
					
	# Wait for the pump to warm up, then turn it off
	#~ @fail_on_timeout(timeout_hours=3.0)
	def heatPump(self):
		
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		PUMP = self.get_config('NAME_PUMP')
		T_WARM_TARGET_PUMP = self.get_config('T_WARM_TARGET_PUMP')
		
		if self.pump_power_current < self.pump_power_target:
			if (time.time() - self.pump_power_time) >= self.pump_power_dt:
				self.pump_power_time = time.time()
				self.pump_power_current += self.pump_power_dp
				self.set_power(PUMP_HTR, self.pump_power_current)
				self.show_status('Heating up the pump slowly, at %0.1f mW' % (1000*self.pump_power_current), add_to_log = False)
		else:
			self.show_status('Waiting for the pump to warm to ' + str(T_WARM_TARGET_PUMP) +' K...', add_to_log = False)

		if self.get_temperature(PUMP) > T_WARM_TARGET_PUMP:		
			self.show_status('Pump has warmed to ' + str(T_WARM_TARGET_PUMP) + ' K.')
			self.move_to_next_step()	
			
			
	# Turn down the pump
	def pumpMaintain(self):
		
		P_MAINTAIN_PUMP_HTR = self.get_config('P_MAINTAIN_PUMP_HTR')	
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		
		self.set_power(PUMP_HTR, P_MAINTAIN_PUMP_HTR)
		self.show_status('Pump heater set to maintain the target temperature')
		
		self.move_to_next_step()

	# Wait for Evap to cool down.  We used to do this by temperature
	# but it caused too many problems.  Time is more reliable.
	'''
	@skip_on_timeout(timeout_hours=4.0)
	def condenseEvap(self):
		
		T_COOL_TARGET_EVAP = self.get_config('T_COOL_TARGET_EVAP_CONDENSE')
		
		self.show_status('Waiting for Evap to cool to ' + str(T_COOL_TARGET_EVAP) + ' K...', add_to_log = False)
		
		EVAP = self.get_config('NAME_EVAP')
		
		if self.get_temperature(EVAP) < T_COOL_TARGET_EVAP:
			self.show_status('Evap has cooled to ' + str(T_COOL_TARGET_EVAP) + ' K')
			self.move_to_next_step()
	'''
	@pause_cycle(duration_seconds = 1.0*60*60)
	def condenseEvap(self, time_remaining="?"):
		self.show_status('Waiting to condense with a hot pump...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
		
	#@pause_cycle(duration_seconds = 90*60)
	#def pauseCondense(self, time_remaining="?"):
	#	self.show_status('Waiting 90 minutes with warm pump...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)

	# Disengage the Evap-1K heat switch 
	def evap1KHeatSwitchOff(self):
			
		HS_EVAP_1K_HTR = self.get_config('NAME_HS_EVAP_1K_HTR')
		
		self.output_off(HS_EVAP_1K_HTR)
		self.show_status('Evap-1K HS heater turned off.')
		
		self.move_to_next_step()
	
	# Wait for the Evap-1K HS to cool, then start pumping
	def waitForEvapToCool(self):
		
		T_COOL_TARGET_HS_EVAP_1K = self.get_config('T_COOL_TARGET_HS_EVAP_1K')
		HS_EVAP_1K = self.get_config('NAME_HS_EVAP_1K')
		
		self.show_status('Waiting for the Evap-1K HS to cool to ' + str(T_COOL_TARGET_HS_EVAP_1K) + ' K...', add_to_log = False)
		
		if self.get_temperature(HS_EVAP_1K) < T_COOL_TARGET_HS_EVAP_1K:
			self.show_status('Evap-1K HS cooled to ' + str(T_COOL_TARGET_HS_EVAP_1K) + ' K.')
			self.move_to_next_step()
			
	# Start pumping
	def startPumping(self):

		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		self.output_off(PUMP_HTR)
		self.show_status('Pump heater off.')

		self.hs_power_current = 0
		self.hs_power_target = self.get_config('P_HEAT_HS_PUMP_4K_FULL')
		self.hs_power_time = time.time()
		self.hs_power_dt = 90 # sec
		self.hs_power_dp = 0.001e-3 # W

		HS_PUMP_4K_HTR = self.get_config('NAME_HS_PUMP_4K_HTR')
		self.set_power(HS_PUMP_4K_HTR, self.hs_power_current)
		self.show_status('Heating up the Pump to 4K heat switch slowly')
		
		self.move_to_next_step()

	# Slowly raise the hs temperature
	def pumpSlowly(self):
		
		if (time.time() - self.hs_power_time) >= self.hs_power_dt:

			HS_PUMP_4K_HTR = self.get_config('NAME_HS_PUMP_4K_HTR')		
			PUMP = self.get_config('NAME_PUMP')

			self.hs_power_time = time.time()
			self.hs_power_current += self.hs_power_dp
			self.set_power(HS_PUMP_4K_HTR, self.hs_power_current)
			self.show_status('Heating up the Pump to 4K heat switch slowly, at %0.3f mW' % (1000*self.hs_power_current), add_to_log = False)
			
			if self.hs_power_current >= self.hs_power_target or self.get_temperature(PUMP) < 25:
				self.show_status('Finished ramping the Pump to 4K heat switch')
				self.move_to_next_step()
	
	# Turn the pump on completely	
	def pumpFully(self):
		HS_PUMP_4K_HTR = self.get_config('NAME_HS_PUMP_4K_HTR')
		self.set_power(HS_PUMP_4K_HTR, self.hs_power_target)
		self.show_status('Finished heating up the Pump to 4K heat switch')
		self.move_to_next_step()

	# Wait for Evap to cool
	def coolingEvap(self):
		
		EVAP = self.get_config('NAME_EVAP')
		T_COOL_TARGET_EVAP = self.get_config('T_COOL_TARGET_EVAP_FINAL')
		
		self.show_status('Waiting for Evap to cool to ' + str(T_COOL_TARGET_EVAP) + ' K...', add_to_log = False)
		
		if self.get_temperature(EVAP) < T_COOL_TARGET_EVAP:
			self.show_status('Evap has cooled to ' + str(T_COOL_TARGET_EVAP) + ' K.')			
			self.move_to_next_step()
	
	@pause_cycle(duration_seconds = 15*60)
	def pauseSettle(self, time_remaining="?"):
		self.show_status('Waiting 15 minutes to settle...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)

	# Connect Evap to the mK stage
	def connectMkStage(self):

		HS_PUMP_4K_HTR = self.get_config('NAME_HS_PUMP_4K_HTR')
					
		HS_EVAP_mK_HTR = self.get_config('NAME_HS_EVAP_mK_HTR')
		P_HEAT_HS_EVAP_mK = self.get_config('P_HEAT_HS_EVAP_mK')
		
		self.set_power(HS_EVAP_mK_HTR,  P_HEAT_HS_EVAP_mK)
		self.show_status('Heating up the Evap to 300 mK heat switch.')
		
		self.wait_time_sec_start = time.time()
		self.cycle_iteration += 1
		
		self.move_to_next_step()
		
	# Wait to start the next cycle
	def waitForNextCycle(self):
		
		wait_time_sec = self.get_config('WAIT_BETWEEN_CYCLES_HR') * 3600
		time_remaining_sec = (wait_time_sec - (time.time() - self.wait_time_sec_start))
		
		if time_remaining_sec <= 0:
			self.move_to_next_step()
		else:
			self.show_status('Waiting to start next cycle (' + str(round(time_remaining_sec/3600,2)) + ' hr remaining)', add_to_log=False)
	
	# Begin the next iteration
	def startNextCycle(self):
		
		if self.current_fridge == 'IC 1':
			self.current_fridge = 'IC 2'
		else:
			self.current_fridge = 'IC 1'
	
		# Go back to the beginning, just after initializing the cycle
		# and just before initializing the iteration
		self.show_status('Beginning next IC cycle iteration...')
		self.move_to_step(self.STEP_START_IC)
		
			
	#########  Event Hooks  ######### 
			
	# A function that is called when the fridge cycle fails.  Use it to set things to a safe state.
	def failure_cleanup(self):
		self.disable_fridge_heaters()
		
	#########  Local Helper Functions  ######### 
		
	def disable_fridge_heaters(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.output_off('Evap 1 Heater')
		self.output_off('Evap 2 Heater')
		self.check_and_disable(self.get_config('NAME_HS_PUMP_4K_HTR'))
		self.check_and_disable(self.get_config('NAME_HS_EVAP_1K_HTR'))
		self.check_and_disable(self.get_config('NAME_HS_EVAP_mK_HTR'))
		self.check_and_disable(self.get_config('NAME_PUMP_HTR'))
		self.check_and_disable(self.get_config('NAME_EVAP_HTR'))
		
		self.show_status('All voltages outputs for this fridge have been set to 0.')
		
	def check_and_disable(self, name):
		if name != None:
			self.output_off(name)
		
		
