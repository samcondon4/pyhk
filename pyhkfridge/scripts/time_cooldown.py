# TIME Cooldown v1.0

# 2017-01-16 JRH - Modified from main fridge cycle
# 2018-01-31 ATC - Modified the TIME fridge cycle to heat pumps and switches on cooldown



import time
import os

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):

	def get_steps(self):
		return [self.initialize,
				self.step_wait_uc,
				self.step_wait_ic_all]

	
	#########  Init Cooldown  #########  
	
	# Initialize the cycle
	def initialize(self):
		self.show_status('Setting all voltages to 0')
		
		for i in [1,2]:
			self.output_off('Evap %i Heater' % i)
		for i in [1,2,3]:
			self.output_off('Pump %i Heater' % i)
		for i in range(1,8):
			self.output_off('HS %i Heater' % i)
		self.output_off('Fridge UC Heater')
		self.output_off('FPU Heater')
				
		self.move_to_next_step()
		

	#########  Check Temps and Wait  ######### 
	
	# Wait and Heat
	def step_wait_uc(self):
		#self.use_config_section(self.current_fridge)

		self.show_status('Waiting for HS7 (uc pump sw) to cool to 14K...', add_to_log = False)

		if (self.get_temperature('HS 7') < 14):

			self.show_status('UC heatswitch (HS 7) have cooled to 14K.')

			self.set_power('Pump 3 Heater',  35e-3)
			self.show_status('Set UC pump to 35 mW.')

			self.move_to_next_step()

	def step_wait_ic_all(self):

		self.show_status('Waiting for HS1 and HS4 (ic pump sws) to cool to 14K...', add_to_log = False)

		if (self.get_temperature('HS 1') < 14) and (self.get_temperature('HS 4') < 14):

			self.show_status('IC heatswitches (HS 1 and 4) have cooled to 14K.')

			# set the IC pump heaters on
			self.set_power('Pump 1 Heater',  12e-3)
			self.show_status('Set IC pump 1 to 12 mW.')

			self.set_power('Pump 2 Heater',  15e-3)
			self.show_status('Set IC pump 2 to 15 mW.')

			# turn on all the other switches to get the fridge in equilibrium at 4K
			self.set_power('HS 2 Heater',  0.9e-3)
			self.show_status('Set HS 2 Heater to 0.9 mW.')

			self.set_power('HS 5 Heater',  0.9e-3)
			self.show_status('Set HS 5 Heater to 0.9 mW.')

			self.set_power('HS 3 Heater',  1.5e-3)
			self.show_status('Set HS 3 Heater to 1.5 mW.')

			self.set_power('HS 6 Heater',  1.5e-3)
			self.show_status('Set HS 6 Heater to 1.5 mW.')

			self.move_to_next_step()


