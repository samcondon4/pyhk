import subprocess
import os
import time

from pyhkfridgelib.fridge_script_base import *
from pyhkdremote.data_loader import pyhkd_get_config_dir

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		return [self.step_heat,
				self.step_wait,
				self.step_cool]
	
	####################################################################
		
	def step_heat(self):
		
		self.set_module_heaters(0.7)
		
		subprocess.call(['ssh time@time-mce-0 "bias_tess 30000 && sleep 30 && bias_tess 2000" &'], shell=True)
		
		self.show_status('Heating FPU...')
				
		self.move_to_next_step()
	
	def step_wait(self):
		
		therm = 'FPU #3 Carbon Array'
		target = 0.475 # K
		
		self.show_status('Waiting for %s to warm to %i mK...' % (therm, 1000*target), add_to_log = False)
		
		if (self.get_temperature(therm) > target):
			self.show_status('FPU heating finished!')
			self.move_to_next_step()

	def step_cool(self):
		
		self.set_module_heaters(0)
		self.show_status('Heaters off!')
				
		self.move_to_next_step()
		
	####################################################################
	
	def set_module_heaters(self, voltage):
		
		for i in range(6):
			self.set_voltage('Module %i Heater' % i,  voltage)
