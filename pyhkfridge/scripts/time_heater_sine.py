import datetime
import numpy as np
import time
import subprocess

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	HEATER_NAME = "FPU Heater"
	SINE_AMP = 0.03 # V
	SINE_PERIOD = 600 # sec
	SINE_OFFSET = 0.1 # V

	def get_steps(self):
		return [self.step_init,
				self.step_run]
	
	def step_init(self):
		self.show_status("Starting infinite sine wave")
		self.sine_t0 = time.time()
		self.last_v = 0
		self.move_to_next_step()
	
	def step_run(self):
		# Sine wave in power space with the given voltage amplitude
		t = time.time() - self.sine_t0
		A = self.SINE_AMP**2
		offset = max(A/2, self.SINE_OFFSET**2)
		v = np.sqrt(offset + A * np.sin(2 * np.pi * t / self.SINE_PERIOD))
		v = round(v, 4) 
		if v != self.last_v:
			self.last_v = v
			self.show_status("Set to %0.4f V" % v, add_to_log=False)
			self.set_voltage(self.HEATER_NAME, v)		

	
