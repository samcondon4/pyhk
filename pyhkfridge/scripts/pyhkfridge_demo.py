# pyhkfridge demo script.  This does not show all of the
# features, just a basic set.  (I encourage you to look
# at, for example, the TIME or SK fridge cycles if you
# are interested in more advanced features.)

import datetime
import numpy as np
import time

from pyhkfridgelib.fridge_script_base import *

# All fridge scripts must implement a class named FridgeScript
# that inherits from FridgeScriptBase
class FridgeScript(FridgeScriptBase):

	# One can define any global constants here
	THERMOMETER_NAME = "4K Head"
	HEATER_NAME = "FPU Heater"

	# FridgeScript.get_steps() should return a list of 
	# functions that define the steps in order.  They
	# can have arbitrary names, and functions can be
	# reused for different steps. Each step should
	# be a quick operation that returns in no more
	# than ~5 seconds.  The current step is called
	# at most once per second until the controller
	# is told to switch steps.
	def get_steps(self):
		# Note: functions should be named something more descriptive!
		return [self.do_a_thing,
			self.wait_to_cool,
			self.step2,
			self.step_heater_wait,
			self.step4]
				
	# This is the first step (because it is the first
	# step in the list returned by get_steps(), NOT
	# because of its name). 
	def do_a_thing(self):

		# Here I add a message to the log file.  This shows
		# under "Status" on the fridge control webpage,
		# and is added to the log.  The log is saved and
		# is viewable on the website.
		self.show_status('Starting demo, turning off heater ' + self.HEATER_NAME)
		
		# Set a voltage to 0V.  Check fridge_script_base.py
		# for other available set functions (like set_power).
		self.set_voltage(self.HEATER_NAME, 0.0)
		
		# Tell the fridge script controller to move to
		# the next step.  This step will not be called
		# again.  In this case, wait_to_cool is the 
		# next step and will be called in ~1 sec.
		self.move_to_next_step()

	# This is the second step
	def wait_to_cool(self):

		# Here I check a temperature.  Check fridge_script_base.py
		# for other available get functions (like get_voltage).
		t = self.get_temperature(self.THERMOMETER_NAME)
		
		# Show a status message without adding it to the log.
		# This helps keep the log from being cluttered with
		# messages that get repeated a lot.
		self.show_status('Waiting on temperature, currently %f...' % t, add_to_log=False)
		
		# We only call move_to_next_step after a certain condition
		# is met.  This means that this function (wait_to_cool)
		# will be called repeatedly (about once per second)
		# until the condition is met.  Because the function
		# returns between each check, the script controller
		# is able to stop/skip the step and update the status.
		# If you do an infinite loop here (if the function never
		# returns), the script will work but the control interface
		# will appear to be frozen.
		if t < 10:
			self.show_status('Temperature goal reached')
			self.move_to_next_step()

	
	def step2(self):
		
		self.show_status('Turning on heater')
		self.set_voltage(self.HEATER_NAME, 0.1)
		self.move_to_next_step()

	# Pause 5 min in a way that shows the user the time countdown
	@pause_cycle(duration_seconds = 5*60)
	def step_heater_wait(self, time_remaining="?"):
		self.show_status('Waiting 5 min with the heater on...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
	
	def step4(self):
		
		self.show_status('Turning heater off.')
		self.set_voltage(self.HEATER_NAME, 0.0)
		
		self.show_status('Demo complete!')
		
		# There is no next step, so moving to the next 
		# step causes the cycle to complete (successfully)
		self.move_to_next_step()
	

