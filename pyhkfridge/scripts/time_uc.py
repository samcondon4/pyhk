# TIME Fridge Cycle v0.3

# 2015-02-23 JRH - Initial code
# 2015-03-04 JRH - Added config file
# 2016-01-25 JRH - Added UC support
# 2017-07-26 ATC - Made one file just for the UC



import time
import os

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	CONFIG_FILE = 'time_fridgecycle.conf'

	STEP_START_IC = 1
	STEP_END_IC = 13
	STEP_START_UC = 14
	
	def get_steps(self):
                return [self.initializeUC,
						self.waitForIC,
						self.coolSwitches,
						self.pumpOn,
						self.heatPump,
						self.pumpOff]
	
	#########  Cycle Init  #########  
	
	
	#########  UC Cycle Steps  ######### 
	
	# Initialize the UC
	def initializeUC(self):
		self.show_status('Beginning UC cycle...')
		self.current_fridge = 'UC'
		self.use_config_section(self.current_fridge)
		
		# Check if we are enabled
		if not self.get_config('ENABLED'):
			self.show_status(self.current_fridge + ' cycle is disabled, skipping.')
			self.complete_script()
		
		self.disable_fridge_heaters()
		self.move_to_next_step()
		
	# Don't run the UC if the IC is mid cycle
	@skip_on_timeout(timeout_hours=6.0)
	def waitForIC(self):
		
		self.move_to_next_step()
		
		# ~ self.show_status('Waiting for the IC Evap to Box HS to turn on so the cycles do not interfere...', add_to_log = False)
		
		# ~ # Make sure both pumps are being cooled, and at least one evap is connected to the box
		# ~ if self.get_voltage('HS 1 Heater') > 0.1 and self.get_voltage('HS 4 Heater') > 0.1:	
			# ~ if self.get_voltage('HS 3 Heater') > 0.1 or self.get_voltage('HS 6 Heater') > 0.1:	
				# ~ self.move_to_next_step()	
	
	# Wait for the Pump-4K switches to cool
	def coolSwitches(self):
		
		HS_PUMP_4K = self.get_config('NAME_HS_PUMP_4K')
		T_COOL_TARGET_HS = self.get_config('T_COOL_TARGET_HS_PUMP_4K')
		
		self.show_status('Waiting for the UC Pump-4K heatswitch to cool to ' + str(T_COOL_TARGET_HS) + ' K...', add_to_log = False)
		
		if (self.get_temperature(HS_PUMP_4K) < T_COOL_TARGET_HS):
			self.show_status('UC Pump-4K heatswitch cooled to to ' + str(T_COOL_TARGET_HS) + ' K.')
			self.move_to_next_step()
					
	# Heat up the pump
	def pumpOn(self):	
		
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		P_HEAT_PUMP_HTR = self.get_config('P_HEAT_PUMP_HTR')
		
		self.set_power(PUMP_HTR, P_HEAT_PUMP_HTR)
		self.show_status('Putting ' + str(P_HEAT_PUMP_HTR*1000) + ' mW on the UC pump heater.')
		
		self.move_to_next_step()
			
	# Wait for the pump to warm up, then turn it off
	@fail_on_timeout(timeout_hours=6.0)
	def heatPump(self):
		
		PUMP = self.get_config('NAME_PUMP')
		T_WARM_TARGET_PUMP = self.get_config('T_WARM_TARGET_PUMP')
		
		self.show_status('Waiting for the UC pump to warm to ' + str(T_WARM_TARGET_PUMP) +' K...', add_to_log = False)
		
		if self.get_temperature(PUMP) > T_WARM_TARGET_PUMP:		
			self.show_status('Pump has warmed to ' + str(T_WARM_TARGET_PUMP) + ' K.')
			self.move_to_next_step()
			
	# Turn off the pump heater, cool it down
	def pumpOff(self):
					
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		
		self.output_off(PUMP_HTR)
		self.show_status('UC pump heater off.')
		
		P_HEAT_HS_PUMP_4K = self.get_config('P_HEAT_HS_PUMP_4K_FULL')
		HS_PUMP_4K_HTR = self.get_config('NAME_HS_PUMP_4K_HTR')
		
		self.set_power(HS_PUMP_4K_HTR, P_HEAT_HS_PUMP_4K)
		self.show_status('Heating up the Pump to 4K heat switch.')
		
		self.move_to_next_step()
	
			
	#########  Event Hooks  ######### 
			
	# A function that is called when the fridge cycle fails.  Use it to set things to a safe state.
	def failure_cleanup(self):
		self.disable_fridge_heaters()
		
	#########  Local Helper Functions  ######### 
		
	def disable_fridge_heaters(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.output_off('Evap 1 Heater')
		self.output_off('Evap 2 Heater')
		self.check_and_disable(self.get_config('NAME_HS_PUMP_4K_HTR'))
		self.check_and_disable(self.get_config('NAME_HS_EVAP_1K_HTR'))
		self.check_and_disable(self.get_config('NAME_HS_EVAP_mK_HTR'))
		self.check_and_disable(self.get_config('NAME_PUMP_HTR'))
		self.check_and_disable(self.get_config('NAME_EVAP_HTR'))
		
		self.show_status('All voltages outputs for this fridge have been set to 0.')
		
	def check_and_disable(self, name):
		if name != None:
			self.output_off(name)
		
		
