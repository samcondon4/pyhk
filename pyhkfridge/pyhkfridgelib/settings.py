

import os

##### PyHK Fridge Settings ##### 

# Don't use /var/tmp
# https://stackoverflow.com/questions/12206857/php-cant-read-file-on-var-tmp

VERSION_STR = 'v3.0'
FRIDGE_SCRIPT_FOLDER = './scripts/'
FRIDGE_STATE_BASE_FOLDER = '/data/hk/tmp/pyhkfridge'
FRIDGE_LOG_BASE_FOLDER = '/data/hk'
APP_LOG_BASE_FOLDER = '/var/log/pyhk'
APP_LOG_FILENAME_INIT = 'pyhkfridge.log'
APP_LOG_FILENAME_PREFIX = 'pyhkfridge-port'
APP_LOG_FILENAME_SUFFIX = '.log'
APP_LOG_FORMAT = '[%(asctime)s] %(levelname)s: %(message)s'
PYHKFRIDGE_PROCNAME = 'pyhkfridge'
PYHKFRIDGE_IP = "localhost"
PYHKFRIDGE_BASE_PORT = 8400
PYHKFRIDGE_MAX_PORT = PYHKFRIDGE_BASE_PORT + 30
COMMON_CODE_DIR = os.path.abspath(os.path.join(__file__,'..','..','..','common'))

# Make sure the folders exists
for f in [FRIDGE_STATE_BASE_FOLDER, FRIDGE_LOG_BASE_FOLDER, APP_LOG_BASE_FOLDER]:
	try: 
		os.makedirs(f)
	except OSError:
		if not os.path.isdir(f):
			raise
