'''
This class is the base class that must be inherited by your fridge script
steps.

Usage:
	- Inherit from FridgeScriptBase
	- Define functions Step_0, Step_1, ..., Step_N
'''



import traceback
import logging
import time
import configparser
import os
import ast

from pyhkdremote.data_loader import pyhkd_get_latest
from pyhkdremote.control import pyhkd_set_voltage, pyhkd_set_power, pyhkd_set_temperature, pyhkd_set_current, pyhkd_set_currentramp, pyhkd_set_state
from pyhkdremote.settings import DATA_LOG_FOLDER

from livecfg.livecfg import LiveCfg

__all__ = ['FridgeScriptBase', 'fail_on_timeout', 'skip_on_timeout', 'pause_cycle']

class FridgeScriptBase:
		
	CONFIG_FILE = None # Don't load a config file
	DEFAULT_CONFIG_HEADER = None # Use the first header by default
	
	def __init__(self, controller, config_dir=''):
		
		self._controller = controller
		self.steps = self.get_steps()
		
		if self.CONFIG_FILE is not None:
			self.CONFIG_FILE = os.path.join(config_dir, self.CONFIG_FILE)
			self.config_reload()
			self.use_config_section(self.DEFAULT_CONFIG_HEADER)
		else:
			self._controller.write_fridge_state('conf_file', '')
			self._controller.write_fridge_state('text_conf', '')
	
	# Returns an ordered list of script step functions
	def get_steps(self):
		return []

	# Returns True if successful
	def execute_step(self, step_num):
	
		#self.show_status("Calling Fridge Script Step " + str(int(step_num)), add_to_log=False)
		try:
			self.steps[step_num]()
			return True
		except Exception as err:
			#logging.error('Contained Fridge Script Error, Step ' + str(int(step_num)) + ': ' + str(traceback.format_exc()))
			self.show_status("Fridge Script Function " + str(int(step_num)) + " Had a Fatal Error, See Log Output")
			self.log('Contained Fridge Script Error, Step ' + str(int(step_num)) + ':\n' + str(traceback.format_exc()).rstrip())
			return False
		
		return False
		
	############  Function hooks that can be overridden by subclasses  ############
	
	# A function that is called when the fridge script fails.  Use it to set things to a safe state.
	def failure_cleanup(self):
		pass
		
	############  Helper functions for use when writing fridge scripts  ############
	
	# Reload the config file from disk
	def config_reload(self):
		if (self.CONFIG_FILE is None) or (not os.path.isfile(self.CONFIG_FILE)):
			self.show_status("Config file loading failure!  Invalid file path " + str(self.CONFIG_FILE))
			raise ValueError("CONFIG_FILE needs to be set to a valid file path!  Given: " + str(self.CONFIG_FILE))
		
		self._config = configparser.SafeConfigParser()
		self._config.read(self.CONFIG_FILE)
		self.show_status("Config file loaded from disk (" + str(self.CONFIG_FILE) + ")")
		self._controller.write_fridge_state('conf_file', os.path.abspath(self.CONFIG_FILE))
		
		with open(self.CONFIG_FILE, 'r') as f:
			text_conf = f.read()
		self._controller.write_fridge_state('text_conf', text_conf)
		
	# Set the config file header to use when loading config variables
	def use_config_section(self, section):
		self._config_current_header = section
	
	# Load the configuration file and pull from the current config heading.
	def get_config(self, key):
		
		if self._config_current_header is not None:
			header = self._config_current_header
		else:
			header = self._config.sections()[0]
		
		# Safely parse the value so we can  correctly return numbers/strings/etc when expected
		return ast.literal_eval(self._config.get(header, key))
	
	# Terminate the script in a failure
	def script_failure(self, reason='Unspecified'):
		self.log('Fridge Script Failure, Running Cleanup...')
		self.failure_cleanup()
		self._controller.fail_script(reason) 
	
	# Terminate the script in a success
	def script_complete(self):
		self._controller.complete_script()   
	
	def script_restart(self):
		self._controller.move_to_step(0)  
	
	def move_to_next_step(self):
		self._controller.move_to_next_step()  
	
	def move_to_step(self, new_step):
		self._controller.move_to_step(new_step) 
	
	# Add something to the fridge script log
	def log(self, text):
		self._controller.log(text)
		
	# Show 'text' as the current status.  If log == True, also add it to the log
	def show_status(self, text, add_to_log = True):
		self._controller.set_status(text, add_to_log)
		
	def set_voltage(self, name, new_voltage):
		pyhkd_set_voltage(name, new_voltage)
	
	def set_power(self, name, new_power):
		pyhkd_set_power(name, new_power)
		
	def set_current(self, name, new_current):
		pyhkd_set_current(name, new_current)
		
	def set_currentramp(self, name, new_currentramp):
		pyhkd_set_currentramp(name, new_currentramp)
	
	def set_devicestate(self, name, new_state):
		pyhkd_set_state(name, new_state)
		
	# Return the value of the temperature in K, or None if it can't be found
	def get_temperature(self, name):
		ts,val = pyhkd_get_latest(DATA_LOG_FOLDER, 'temperature', name)
		return val
		
	# Return the value of the power in W, or None if it can't be found
	def get_power(self, name):
		ts,val = pyhkd_get_latest(DATA_LOG_FOLDER, 'power', name)
		return val
		
	# Return the value of the voltage in V, or None if it can't be found
	def get_voltage(self, name):
		ts,val = pyhkd_get_latest(DATA_LOG_FOLDER, 'voltage', name)
		return val
		
	# Return the value of the current in A, or None if it can't be found
	def get_current(self, name):
		ts,val = pyhkd_get_latest(DATA_LOG_FOLDER, 'current', name)
		return val
		
	# Return the value of the resistance in Ohms, or None if it can't be found
	def get_resistance(self, name):
		ts,val = pyhkd_get_latest(DATA_LOG_FOLDER, 'resistance', name)
		return val
		
	# Return the value of the temperature derivative in K/s, or None if it can't be found
	def get_tempderiv(self, name):
		ts,val = pyhkd_get_latest(DATA_LOG_FOLDER, 'tempderiv', name)
		return val
		
	# Turn off a heater
	def output_off(self, name):
		self.set_voltage(name, 0)
		
	def start_temperature_regulation(self, name, target):
		pyhkd_set_temperature(name, target)
	
	# Enables or disables LS370 thermometer channels.
	# Returns True iff it completed successfully.
	# fname: the path to the live config file
	# enabled: a list of 16 values corresponding to each channel
	#			True: enable
	#			False: disable
	#			None: don't change
	def ls370_set_enabled(self, fname, enabled):
		
		assert(len(enabled) >= 16)
		
		try:
			
			# Load the LS370 data
			lc = LiveCfg(fname)
			data = lc.load()
			
			if data is None:
				self.log('Failed to load LS370 settings!  Can\'t enable any disabled thermometers.')
				return False
			
			# Enable/disable thermometers
			for i in range(16):
				if (enabled[i] == True) or (enabled[i] == False):
					data[i]['enabled'] = enabled[i]
				else:
					pass
			
			# Write the LS370 data
			lc.dump(data)
			
		except:
			self.log('Failed to set LS370 settings!  Contained error message:\n' + str(traceback.format_exc()).rstrip())
			return False
			
		return True
			
	
############  Step decorators  ############
	
# Tells a step to end the fridge script if the timeout
# is reached without changing steps
def fail_on_timeout(timeout_seconds = 0, timeout_hours = 0):
	timeout_seconds += timeout_hours * 3600
	def generated_decorator(func):
		def wrapper(self, *args, **kwargs):
			
			if (time.time() - self._controller.step_start_time) >= timeout_seconds:
				self.script_failure('Timeout reached for ' + func.__name__)
				return
				
			func(self, *args, **kwargs)
		wrapper.__name__ = func.__name__
		return wrapper
	return generated_decorator
	
# Tells a step to skip forward if the timeout
# is reached without changing steps
def skip_on_timeout(timeout_seconds = 0, timeout_hours = 0):
	timeout_seconds += timeout_hours * 3600
	def generated_decorator(func):
		def wrapper(self, *args, **kwargs):
			
			if (time.time() - self._controller.step_start_time) >= timeout_seconds:
				self.show_status('Timeout reached for ' + func.__name__ + ', forcibly skipping this step')
				self.move_to_next_step()
				return
				
			func(self, *args, **kwargs)
		wrapper.__name__ = func.__name__
		return wrapper
	return generated_decorator

# Turn this step into a pause 
def pause_cycle(duration_seconds = 0, duration_hours = 0):
	duration_seconds += duration_hours * 3600
	def generated_decorator(func):
		def wrapper(self, *args, **kwargs):
							
			tr = duration_seconds - (time.time() - self._controller.step_start_time)
			
			if tr <= 0:
				self.show_status('Pause time has elapsed, moving on.')
				self.move_to_next_step()
				return
			else:
				func(self, time_remaining=tr, *args, **kwargs)
		wrapper.__name__ = func.__name__
		return wrapper
	return generated_decorator
