'''
Interfaces with an Arduino conected via serial running the firmware
included in the firmware folder.  This can be expanded to support
other firmware using the same command structure.
'''

import os
import logging
import time
import serial
import struct

from pyhkdlib.sensor import Sensor
from pyhkdlib.instruments.serial_instrument import SerialInstrument

class Arduino(SerialInstrument):
	
	NUM_SENSORS = None
	
	STATE_NO_CHANGE = -1
	STATE_TURN_OFF = 0
	STATE_TURN_ON = 1
	
	PKT_START = '<'
	PKT_DELIM = ','
	PKT_END = '>'
	
	# Long wait time because changes are reported right away
	def __init__(self, port, wait_time = 60, **kwargs):
		
		# This class requires rework and is not functional currently.
		# In addition, need to change "pin" to "id" in config.
		raise Exception()
		
		self.last_update_time = 0
		self.wait_time = wait_time
				
		SerialInstrument.__init__(self, 
			port, 
			baudrate = 9600, 
			pkt_end = self.PKT_END,
			pkt_start = self.PKT_START,
			default_sensor_type = Sensor.TYPE_STATE,
			**kwargs)
			
		pins = [c.get('pin', None) for c in self.channels]
		self.index_lookup_pin = {pins[i] : i for i in range(self.NUM_SENSORS)}

	def handle_packet(self, msg):
		
		msg = msg.split(self.PKT_DELIM)
		
		if len(msg) < 1:
			return
			
		cmd = msg[0]
		
		# Firmware error
		if cmd == 'error':
			
			logging.error("Arduino firmware reported an error: " + str(msg))
		
		# Firmware boot
		elif cmd == 'boot':
			
			logging.debug("Arduino firmware booted")
		
		# Pin status
		elif cmd == 'd':
			
			try:
				pin = int(msg[1])
				val = int(msg[2])
			except (IndexError, ValueError) as e:
				logging.error("Invalid packet from Arduino firmware: " + str(msg))
				return
				
			# Store the value
			sensor_index = self.index_lookup_pin.get(pin, None)
			if sensor_index is not None:
				self.sensor[sensor_index].value = val
				
			#~ logging.debug("Arduino reports pin %i is set to state %i" % (pin, val))
		
		else:
			
			logging.error("Unrecognized packet from Arduino firmware: " + str(msg))
		
	# Send a pin read request
	def _read_pin(self, pin):
		
		msg = "din"
		msg += self.PKT_DELIM
		msg += str(pin)
		
		self.send_packet(msg)
		
		#~ logging.debug("Requesting Arduino pin %i state" % (pin))
		
	# Send a pin write request (0 for low, 1 for high)
	def _write_pin(self, pin, value):
		
		assert(value in [0,1])
		
		msg = "dout"
		msg += self.PKT_DELIM
		msg += str(pin)
		msg += self.PKT_DELIM
		msg += str(value)
		
		self.send_packet(msg)
		
		logging.debug("Setting Arduino pin %i to state %i" % (pin, value))
	
	def update(self):

		# Check if certain amount of time has passed since something was read
		if ((time.time() - self.last_update_time) >= self.wait_time):
			
			self.last_update_time = time.time()
			
			# Get status of all the pins
			for c in self.channels:
				pin = c.get('pin', None)
				if pin is not None:
					self._read_pin(pin)
		
		# Check for new targets
		for c in self.channels:
			
			name = c.get('name')
			pin = c.get('pin')
			
			if self.state_targets[name] == self.STATE_TURN_ON:
				self._write_pin(pin, 1)
				self.state_targets[name] = self.STATE_NO_CHANGE
			elif self.state_targets[name] == self.STATE_TURN_OFF:
				self._write_pin(pin, 0)
				self.state_targets[name] = self.STATE_NO_CHANGE
			else:
				pass
			
	
	# Watch external dicts for targets
	def connect_targets(self, targets):
		raise Exception() # this func is obsolete, should be reworked
		# Save references to target dicts
		self.state_targets = targets[Sensor.TYPE_TARGET_STATE]
		
		# Touch all valid names
		for n in self.names:
			self.state_targets[n] = self.STATE_NO_CHANGE

	
