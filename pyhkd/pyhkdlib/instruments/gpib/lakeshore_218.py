'''
Read from a Lakeshore 218
'''

import time
import logging
import serial
import threading

from pyhkdlib.sensor import Sensor
from .gpib_instrument import GPIBSCPIInstrument, SerialSCPIInstrument

from calib.helpers import get_calib

# Control logic shared between the GPIB and Serial interfaces
class AbstractLakeshore218:
	
	NUM_SENSORS = 8
	BOX_TYPE = 'LS218'
	IDN_STR = 'MODEL218'
	
	RATE_LIMIT_TIMEOUT = 3 # sec to wait before assuming connection issues and requesting new data again
	
	def __init__(self):
		
		self._lock = threading.Lock()
		self._last_processed = True
		self._last_processed_time = time.time()
			
	def handle_data(self, response):
		
		if self.verbose_rx:
			logging.debug("LS218 data received")
		
		try:
			
			response_split = response.rstrip().split(",")
			
			# Make sure we have the right numbers
			if len(response_split) != self.NUM_SENSORS:
				raise ValueError()
			
			# Attempt to extract the values
			extracted_raw = [float(response_split[i]) for i in range(self.NUM_SENSORS)]

		except (TypeError, AttributeError, ValueError):
			
			logging.error("Error loading in Lakshore 218 temperatures (received: %s)" % (repr(response)))				
			return
		
		# Save the values
		for i in range(self.NUM_SENSORS):
			self.get_sensor(i, Sensor.TYPE_VOLTAGE).value = extracted_raw[i]
			self.get_sensor(i, Sensor.TYPE_TEMPERATURE).value = self.get_calib_func(i)(extracted_raw[i])
			
		with self._lock:
			self._last_processed = True
			self._last_processed_time = time.time()

	# Code to run when connected with a period of wait_time.
	# Implements AbstractSCPIInstrument.update_connected
	def update_connected(self):
		
		with self._lock:
			if (not self._last_processed) and ((time.time()-self._last_processed_time) < self.RATE_LIMIT_TIMEOUT):
				return # Skip this, we are waiting on the last one still
			self._last_processed = False

		# Read all the temperatures in
		response = self.send_packet('SRDG?', resp_callback=self.handle_data)

class GPIBLakeshore218(AbstractLakeshore218, GPIBSCPIInstrument):
	
	def __init__(self, controller, address, channels=[], wait_time=8, **kwargs):
		
		GPIBSCPIInstrument.__init__(self, controller, address, channels, 
			wait_time = wait_time,
			default_sensor_type = Sensor.MULTI_TYPE_THERM_DIODE, 
			default_calib_func = get_calib('V2T_DT400'),
			**kwargs)
			
		AbstractLakeshore218.__init__(self)
		

class SerialLakeshore218(AbstractLakeshore218, SerialSCPIInstrument):
			
	def __init__(self, port, channels=[], wait_time=8, baudrate=9600, **kwargs):

		SerialSCPIInstrument.__init__(self, 
			port, 
			baudrate = baudrate, 
			bytesize=serial.SEVENBITS, 
			parity=serial.PARITY_ODD, 
			stopbits=serial.STOPBITS_ONE,
			pkt_end = '\r\n',
			pkt_start = None,
			channels = channels,
			default_sensor_type = Sensor.MULTI_TYPE_THERM_DIODE, 
			default_calib_func = get_calib('V2T_DT400'),
			wait_time = wait_time,
			**kwargs)
		
		AbstractLakeshore218.__init__(self)
