'''
Read and control an AMI420
'''

import time
import numpy as np
import logging
import os
import json
import traceback
import math

from .gpib_instrument import AbstractSCPIInstrument, GPIBSCPIInstrument
from pyhkdlib.sensor import Sensor
		
class AMI420(GPIBSCPIInstrument):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'AMI420'
	
	MODE_PAUSE = 0
	MODE_RAMP = 1
	MODE_ZERO = 2
	
	# The mapping between saved sensor types and the command to read
	# that value.  No duplicate types allowed currently.
	READ_COMMANDS = [
			(Sensor.TYPE_VOLTAGE, "voltage:supply?"),
			(Sensor.TYPE_CURRENT, "current:magnet?"),
			(Sensor.TYPE_BFIELD, "field:magnet?"),
			(Sensor.TYPE_STATE, "state?"),
			]

	def __init__(self, controller, address, name_prefix="Magnet", wait_time=5, coil_const=0.4409, current_limit=9, **kwargs):
		
		# Speed up the wait time and spread the request load over it.
		# We request one value per subframe, so the read period per
		# value is still wait_time.
		self.frame_subsamples = len(self.READ_COMMANDS)
		wait_time /= self.frame_subsamples
		self._subframe_num = 0
				
		self.name_prefix = name_prefix
		self._coil_const = coil_const
		self._current_limit = current_limit
		
		assert 'channels' not in kwargs, 'Use name_prefix for AMI420, do not specify channels directly'
		
		# The box has one "channel" (one magnet) with several data types
		typelist = [x[0] for x in self.READ_COMMANDS]
		typelist += [Sensor.TYPE_TARGET_CURRENT, Sensor.TYPE_TARGET_CURRENTRAMP, Sensor.TYPE_TARGET_STATE]
		assert len(typelist) == len(set(typelist)), "Duplicate types specified in AMI420 class, code written assumes this is not the case"
		channels = [ {'name': name_prefix, 'type': typelist} ]
		
		GPIBSCPIInstrument.__init__(self, controller, address, channels, wait_time=wait_time, **kwargs)
				
	# Perform the initial config.
	# Overrides AbstractSCPIInstrument.initial_config.
	def initial_config(self):
		
		AbstractSCPIInstrument.initial_config(self)
	
		self.send_packet("configure:coilconst " + str(self._coil_const))
		self.send_packet("configure:current:limit " + str(self._current_limit))
		self.send_packet("configure:ramp:rate:units 0") # choose A/sec
		self.send_packet("configure:field:units 1") # choose Tesla
		
		self.last_current = None
		self.last_currentramp = None
		self.last_state = None

	# 'current_target' is target current in Amps
	# 'current_rate' is ramp rate in Amps/sec
	def set_current_target(self, current_target, current_rate):
		logging.info("Setting AMI 420 current target to " + str(current_target) + " and ramp rate to " + str(current_rate) + " A/sec")
		self.send_packet("configure:current:program " + str(current_target))
		self.send_packet("configure:ramp:rate:current " + str(current_rate))
	
	def set_state_ramp(self):
		logging.info("Setting AMI 420 to ramp")
		self.send_packet("ramp")
	
	def set_state_zero(self):
		logging.info("Setting AMI 420 to zero")
		self.send_packet("zero")
		
	def set_state_pause(self):
		logging.info("Setting AMI 420 to pause")
		self.send_packet("pause")
	
	# Check for state changes and update accordingly
	def _process_state_targets(self):
		
		targsen_state = self.get_sensor(0, Sensor.TYPE_TARGET_STATE)
	
		if targsen_state.value is None or math.isnan(targsen_state.value):
			return
			
		if (self.last_state != targsen_state.value): 
			
			state = targsen_state.value
			if state == self.MODE_PAUSE:
				self.set_state_pause()
			elif state == self.MODE_RAMP:
				self.set_state_ramp()
			elif state == self.MODE_ZERO:
				self.set_state_zero()
			else:
				logging.debug('Bad state target' + str(self.name_prefix) + ', reset to PAUSED: ' + str(targsen_state.value))
				targsen_state.value = self.MODE_PAUSE
			
			self.last_state = targsen_state.value
		
	# Check for target changes and update accordingly
	def _process_current_targets(self):
		
		targsen_iramp = self.get_sensor(0, Sensor.TYPE_TARGET_CURRENTRAMP)
		targsen_i = self.get_sensor(0, Sensor.TYPE_TARGET_CURRENT)
		
		if targsen_i.value is None or math.isnan(targsen_i.value):
			return
			
		if targsen_iramp.value is None or math.isnan(targsen_iramp.value):
			return

		if (self.last_current != targsen_i.value) or (self.last_currentramp != targsen_iramp.value):
			
			# Validate the targets
			try:
				ramp_target = float(targsen_i.value)
				if ramp_target < 0:
					ramp_target = 0
			except ValueError:
				logging.debug('Bad current target ' + str(self.name_prefix) + ', reset to 0: ' + str(targsen_i.value))
				targsen_i.value = 0
			try:
				ramp_rate = float(targsen_iramp.value)
				if ramp_rate < 0:
					ramp_rate = 0
			except ValueError:
				logging.debug('Bad current ramp target ' + str(self.name_prefix) + ', reset to 0: ' + str(targsen_iramp.value))
				targsen_iramp.value = 0
			
			# Set the target
			self.set_current_target(ramp_target, ramp_rate)
			self.last_current = targsen_i.value
			self.last_currentramp = targsen_iramp.value
			
		
	# Handle a value returned
	def handle_value(self, sensor_type, data_str):
		
		try:
			val = float(data_str)
		except:
			logging.error("Error loading in " + self.SPECIFIC_DEVICE_ID + " value. Read: " + repr(data_str)) 
			return
					
		self.get_sensor(0, sensor_type).value = val


	# Code to run when connected with a period of wait_time.
	# Implements AbstractSCPIInstrument.update_connected
	def update_connected(self):
		
		self._process_current_targets()			
		self._process_state_targets()			

		# Figure out which value we are using this subframe
		(sen_type, command) = self.READ_COMMANDS[self._subframe_num]
		self._subframe_num = (self._subframe_num + 1) % self.frame_subsamples
			
		# The "t=sen_type" syntax is to freeze in the current value
		cb = lambda x, t=sen_type: self.handle_value(t, x)
		self.send_packet(command, resp_callback=cb)
	
	
