import logging
import time
import struct
import numpy as np

from .hkmb_base import HKMBBase

from pyhkdlib.sensor import Sensor


class HKMBv2(HKMBBase):
	BOX_TYPE = 'HKMBv2'
	
	MAX_NUM_CARDS = 6
	CHAN_PER_CARD = 4
	
	def __init__(self, port, channels, serial_num, *args, **kwargs):

		# Add in the diagnostic channels
		name_prefix = self.BOX_TYPE + 'b' + str(serial_num) + '_diag_'
		for conn_i in range(self.MAX_NUM_CARDS):
			for chan_i in range(self.CHAN_PER_CARD):
				# Sensor and monitor overflow and noise
				for s in ['SEN','MON']:
					id_ = "T%i%i%s" % (conn_i, chan_i, s)
					name = name_prefix + id_
					c = {"id": id_, "name": name, "type": [Sensor.TYPE_FRACTION, Sensor.TYPE_VOLTAGE]}
					channels.append(c)
				# Phase
				for s in ['MEAN','DELTA']:
					id_ = "T%i%i%s" % (conn_i, chan_i, s)
					name = name_prefix + id_
					c = {"id": id_, "name": name, "type": Sensor.TYPE_ANGLE}
					channels.append(c)
			# Per-card carrier amplitude
			for s in ['CARRIER']:
				id_ = "C%i%s" % (conn_i, s)
				name = name_prefix + id_
				c = {"id": id_, "name": name, "type": Sensor.TYPE_VOLTAGE}
				channels.append(c)
		
		HKMBBase.__init__(self, port, channels, serial_num, *args, **kwargs)
		
		self._packet_lookup['RES'] = self.handle_packet_res
		self._packet_lookup['AIN'] = self.handle_packet_ain
		self._packet_lookup['EXC'] = self.handle_packet_exc
		self._packet_lookup['RAW'] = self.handle_packet_raw
		self._packet_lookup['INR'] = self.handle_packet_inr
		self._packet_lookup['PHI'] = self.handle_packet_phase
		self._packet_lookup['QNS'] = self.handle_packet_qnoise
		self._packet_lookup['OVF'] = self.handle_packet_ovf

	# Resistors
	def handle_packet_res(self, update_time, syncnum, d):

		for i in range(len(d)):		
			
			conn_i = (i // 4)
			chan_i = (i % 4)
			id_ = "T%i%i" % (conn_i, chan_i)
			
			if id_ not in self.sensor_ids:
				#~ logging.debug("Excess HKMB RES data, index " + str(i))
				continue
			
			calib_func = self.get_calib_func(id_)
					
			try:
				if 'ovf' in d[i]:
					val_r = 0
				else:
					val_r = float(d[i]) # Ohms 
			except:
				logging.error("Bad HKMB resistor packet: " + str(d))
				return
				
			val_t = calib_func(val_r) # K
			
			self.get_sensor(id_, Sensor.TYPE_RESISTANCE).set_value(val_r, update_time, syncnum)
			self.get_sensor(id_, Sensor.TYPE_TEMPERATURE).set_value(val_t, update_time, syncnum)
			
	# Q noise of the sensor and monitor
	def handle_packet_qnoise(self, update_time, syncnum, d):
		func = lambda x: 1e-9*float(x)
		sen_type = Sensor.TYPE_VOLTAGE
		self._unpack_diag(update_time, syncnum, d, func, sen_type)	
			
	# Phase of the sensor and monitor
	def handle_packet_phase(self, update_time, syncnum, d):
		func = lambda x: np.degrees(float(x))
		sen_type = Sensor.TYPE_ANGLE
		self._unpack_diag(update_time, syncnum, d, func, sen_type, suffixes=['MEAN','DELTA'])	
				
	# Overflow metrics for the sensor and monitor
	def handle_packet_ovf(self, update_time, syncnum, d):
		func = float
		sen_type = Sensor.TYPE_FRACTION
		self._unpack_diag(update_time, syncnum, d, func, sen_type)
		
	# Handle the diagnostic packets which provide a value for monitor
	# and sensor for each channel 
	def _unpack_diag(self, update_time, syncnum, d, func, sen_type, suffixes=['MON','SEN']):
				
		len_expect = 2 * self.MAX_NUM_CARDS * self.CHAN_PER_CARD + 1
		if len(d) != len_expect:
			logging.error("Bad HKMBv2 diagnostic packet length (expecting %i, found %i)" % (len_expect, len(d)))
			return
			
		for conn_i in range(self.MAX_NUM_CARDS):
			for chan_i in range(self.CHAN_PER_CARD):
				
				id_ = "T%i%i" % (conn_i, chan_i)
							
				if id_ not in self.sensor_ids:
					continue
				
				# Find the index of the monitor and the sensor values
				ind_mon = 2 * (conn_i * self.CHAN_PER_CARD +  chan_i)
				ind_sen = ind_mon + 1
							
				try:
					val_mon = func(d[ind_mon])
					val_sen = func(d[ind_sen])
				except:
					logging.error("Bad HKMB diagnostic packet: " + str(d))
					return
					
				self.get_sensor(id_ + suffixes[0], sen_type).set_value(val_mon, update_time, syncnum)
				self.get_sensor(id_ + suffixes[1], sen_type).set_value(val_sen, update_time, syncnum)
					
				
	# Excitation for resistors
	def handle_packet_exc(self, update_time, syncnum, d):
				
		len_expect = 2 * self.MAX_NUM_CARDS * (self.CHAN_PER_CARD + 1) + 1
		if len(d) != len_expect:
			logging.error("Bad HKMBv2 exc packet length (expecting %i, found %i)" % (len_expect, len(d)))
			return
	
		for conn_i in range(self.MAX_NUM_CARDS):
			for chan_i in range(self.CHAN_PER_CARD + 1):
								
				# Find the index of the current and dac vals reported
				ind_cur = 2 * (conn_i * (self.CHAN_PER_CARD + 1) + chan_i)
				ind_dac = ind_cur + 1
						
				try:
					if 'ovf' in d[ind_cur]:
						val_exc = 0
					else:
						val_exc = float(d[ind_cur]) 
					val_dac = int(d[ind_dac])
				except:
					logging.error("Bad HKMB exc packet: " + str(d))
					return
					
				if chan_i < self.CHAN_PER_CARD:
					id_ = "T%i%i" % (conn_i, chan_i)
				else:
					id_ = "C%iCARRIER" % (conn_i)
							
				if id_ not in self.sensor_ids:
					logging.debug("Excess HKMBv2 exc data, can't find sensor with ID " + str(id_))
					continue
				
				if chan_i < self.CHAN_PER_CARD:
					
					val_current = val_exc * 1e-9 # A 
					val_r = self.get_sensor(id_, Sensor.TYPE_RESISTANCE).value
					val_v = val_current * val_r
					#~ val_p = val_current * val_v # W
					
					self.get_sensor(id_, Sensor.TYPE_VOLTAGE).set_value(val_v, update_time, syncnum)
					self.get_sensor(id_, Sensor.TYPE_CURRENT).set_value(val_current, update_time, syncnum)
					self.get_sensor(id_, Sensor.TYPE_DAC).set_value(val_dac, update_time, syncnum)
					
				else:
					
					val_v = val_exc * 1e-3 # V RMS
					self.get_sensor(id_, Sensor.TYPE_VOLTAGE).set_value(val_v, update_time, syncnum)
					# ~ logging.debug("Carrier on card %i is %0.4f V RMS" % (conn_i, val_v))
				
	# Analog input
	def handle_packet_ain(self, update_time, syncnum, d):
			
		for i in range(len(d)):		
			
			id_ = "AI%i" % (i)
			
			if id_ not in self.sensor_ids:
				logging.debug("Excess HKMB AIN data, index " + str(i))
				continue
			
			try:
				val = float(d[i]) # volts 
			except:
				logging.error("Bad HKMB AIN packet: " + str(d))
				return

			self.get_sensor(id_, Sensor.TYPE_VOLTAGE).set_value(val, update_time, syncnum)
			
			chan = self.get_channel(id_)
			c_type = chan['type']
			
			if isinstance(c_type, list) and (len(c_type) == 2):
					
				# Manual dual type, voltage to something else.
				# Let's figure out the target type.
				target_type = c_type[0]
				if (target_type == Sensor.TYPE_VOLTAGE):
					target_type = c_type[1]
				
				val_conv = self.get_calib_func(id_)(val)
				self.get_sensor(id_, target_type).set_value(val_conv, update_time, syncnum)
				
	# Raw debugging data
	def handle_packet_raw(self, update_time, syncnum, d):

		nsamp = 48
		nchan = 4
		data = self._unpack_hexprint(d, nsamp, nchan)
		if data is None:
			return
			
		logging.debug("Received RAW packet for voltage channel " + str(d[0]))
			
		sen_ids = ['V','M','C','S']
		dt = 0.001 # Time offset to verify file order
		
		# Save the data
		for i in range(nchan):
			
			sid = "RAW" + sen_ids[i]
			s = self.get_sensor(sid, Sensor.TYPE_ADU, none_on_fail=True)
			if s is None:
				logging.debug("HKMBv2 RAW sensor ID not found: " + sid)
				return
				
			for j in range(nsamp):		
				s.set_value(data[i][j], update_time + j*dt, syncnum)
				
	# Intermediate-frequency debugging data
	def handle_packet_inr(self, update_time, syncnum, d):
		
		nsamp = 8
		nchan = 11
		ntrailing = 4
		data_all = self._unpack_hexprint(d, nsamp, nchan, ntrailing)
		if data_all is None or len(data_all) != 2:
			return
		
		data, trailing = data_all
		
		logging.debug("Received INR packet for voltage channel " + str(d[0]))
		
		sen_ids = ['CR','CI','CQ','MR','MB','MI','MQ','SR','SB','SI','SQ','MIL','MQL','SIL','SQL']
		dt = 0.001 # Time offset to verify file order
		
		# Save the data
		for i in range(nchan+ntrailing):
			
			sid = "INR" + sen_ids[i]
			s = self.get_sensor(sid, Sensor.TYPE_ADU, none_on_fail=True)
			if s is None:
				logging.debug("HKMBv2 INR sensor ID not found: " + sid)
				return
			
			if i < nchan:
				for j in range(nsamp):		
					s.set_value(data[i][j], update_time + j*dt, syncnum)
			else:
				# Single values at the end
				s.set_value(trailing[i-nchan], update_time, syncnum)
	
	# Unpack debugging data in the form of floats hex-printed as 
	# unsigned ints
	def _unpack_hexprint(self, d, nsamp, nchan, ntrailing=0):
		
		nchars = 8 # chars per sample per channel
		expectedvals = nchan * nsamp + ntrailing
		expectedchars = expectedvals * nchars
		
		if len(d) != 2:
			logging.error("Unexpected hex debug packet length: " + str(len(d)))
			return None
		
		data = d[1]
		
		if len(data) != expectedchars:
			logging.error("Unexpected hex debug packet data size.  Received %i, expected %i." % (len(data), expectedchars))
			return None
			
		# Reshape into data[value_num]
		data = [data[nchars*n:nchars*(n+1)] for n in range(expectedvals)]
		
		# Convert hex to an unsigned integer
		try:
			data = [int(d, base=16) for d in data]
		except:
			logging.error("Invalid data found in hex debug packet.")
			return None
		
		# Convert uint32 to float32
		data = [struct.pack('<L',d) for d in data]
		data = [struct.unpack('<f',d)[0] for d in data]
			
		# Reshape into data[chan_num][sample_num]
		if ntrailing > 0:
			trailing = data[-ntrailing:]
		data = [data[nsamp*n:nsamp*(n+1)] for n in range(nchan)]
		
		if ntrailing > 0:
			return data, trailing
		else:
			return data
