import os
import logging
import time
import calendar
import serial
import math
import numpy as np

from collections import OrderedDict

from pyhkdlib.sensor import Sensor
from pyhkdlib.instruments.serial_instrument import SerialInstrument
from pyhkdlib.instruments.voltage_output_mixin import VoltageOutputMixin
from livecfg.livecfg import LiveCfg
from pyhkdremote.data_loader import pyhkd_get_config_dir

from scipy import optimize
from scipy.misc import derivative

class HKMBBase(SerialInstrument, VoltageOutputMixin):
	
	BOX_TYPE = 'HKMB'
	
	FIELD_DELIM = ','
	
	VOLTAGE_UPDATE_PERIOD = 0.5 # Seconds
	HEARTBEAT_PERIOD = 1 # Seconds
	
	SER_TX_PAUSE = 0.01 # seconds to pause between sends
	
	HKMB_PKT_START = "<"
	HKMB_PKT_END = ">\r\n"
	
	def __init__(self, port, channels, serial_num, live_config_filename = None, **kwargs):
		
		self._port = port
		self._serial_num = serial_num
		
		self.last_voltage_update = 0
		self.last_heartbeat = 0
		
		self._packet_lookup = {}
		self._packet_lookup['SEN'] = self.handle_packet_sensor
		self._packet_lookup['FPS'] = self.handle_packet_fps
		self._packet_lookup['SYNC'] = self.handle_packet_sync
		self._packet_lookup['PID'] = self.handle_packet_pid
		self._packet_lookup['ENV'] = self.handle_packet_env
		
		if live_config_filename is None:
			self._cfg_live_filename = None
			self._cfg_live = None
		else:
			# We may have been passed a file name in the default folder
			full_file_path = os.path.join(pyhkd_get_config_dir(), live_config_filename)
			if os.path.exists(live_config_filename):		
				self._cfg_live_filename = live_config_filename
			else:
				self._cfg_live_filename = full_file_path
				
			self._cfg_live = LiveCfg(self._cfg_live_filename)
		
		self.NUM_SENSORS = len(channels)
	
		SerialInstrument.__init__(self, port, baudrate = 230400, pkt_end = self.HKMB_PKT_END, pkt_start = self.HKMB_PKT_START, channels=channels, **kwargs)			
		VoltageOutputMixin.__init__(self, default_max_voltage = 10.5, can_set_power = True, can_set_temperature = True)
		
		self._pid_therm = {}
		
		# Clear out communications so the first real packet works
		for i in range(5):
			self.send_packet("heartbeat")
		
		# Set excitation
		self.fix_live_config()
		self.execute_live_config()		
		
		# Force the ADCs to resync.  Note this disrupts the readout and
		# processing in the firmware so expect weird readings.
		self.send_packet("adcsync")
		
			
	# Implements Instrument.update()
	def update(self):	
						
		# Check if we need to reload the config file
		if self._cfg_live is not None and self._cfg_live.wasModified():
			self.execute_live_config()

		if (time.time() - self.last_voltage_update) >= self.VOLTAGE_UPDATE_PERIOD:
			self.process_voltage_targets()
			self.last_voltage_update = time.time()

		if (time.time() - self.last_heartbeat) >= self.HEARTBEAT_PERIOD:
			self.send_packet("heartbeat")
			self.last_heartbeat = time.time()
		
	# Implements SerialInstrument.handle_packet()
	def handle_packet(self, msg):
		
		l = msg.strip()

		# Empty or single-char lines are garbage
		if len(l) <= 1:
			return
					
		# These are debugging messages from the box firmware
		if l[0] == '#':
			logging.debug(self.BOX_TYPE + " message: " + l[1:])
			return
		
		d = l.split(self.FIELD_DELIM)
		
		if len(d) < 3:
			logging.error("Bad " + self.BOX_TYPE + " message: " + str(d))
			return
		
		cmd = d.pop(0)
		syncnum = d.pop(0)
		
		update_time = time.time()
		
		# Check if this is a global or local sync number
		# TODO: store this information in the data files
		local_sync = False
		if syncnum.startswith("loc"):
			local_sync = True
			syncnum = syncnum[3:]
			
		try:
			syncnum = int(syncnum)
		except:
			syncnum = None
		
		if cmd in self._packet_lookup:
			self._packet_lookup[cmd](update_time, syncnum, d)
		else:
			logging.error("Unhandled " + self.BOX_TYPE + " packet type: " + str(cmd))

	# Temperature target reporting
	def handle_packet_pid(self, update_time, syncnum, d):

		try:
			
			heater = int(d[0])
			assert(heater >= 0)
			
			if d[1] == 'off':
				val = None
			else:
				val = float(d[1])
				
			chan_id = "H%02i" % heater
			s = self.get_sensor(chan_id, Sensor.TYPE_TEMPERATURE)
			s.value = val
				
		except:
			logging.error("Bad " + self.BOX_TYPE + " pid packet: " + str(d))
			return

	def handle_packet_fps(self, update_time, syncnum, d):

		id_ = "FPS"
		
		if id_ not in self.sensor_ids:
			return

		try:
			val = float(d[0]) # Hz
		except:
			logging.error("Bad " + self.BOX_TYPE + " fps packet: " + str(d))
			return
			
		try:
			ram_usage = int(d[1])
			logging.debug("Free RAM: " + str(ram_usage))
		except:
			pass

		self.get_sensor(id_, Sensor.TYPE_FREQUENCY).set_value(val, update_time, syncnum)
			
	# Handles MCE+IRIG sync measurements
	def handle_packet_sync(self, update_time, reporttime_syncnum, d):

		id_ = "SYNC"
		
		if id_ not in self.sensor_ids:
			return
		
		# The syncnum during the measurement, not to be confused with
		# syncnum at the time the packet was sent.
		try:
			syncnum = int(d[0]) 
		except:
			syncnum = None
		
		try:
			update_time_raw = d[1]
			t = time.strptime(update_time_raw, '%Y-%jT%H:%M:%SZ')
			epoch_time = calendar.timegm(t)
			dt_sync = float(d[2]) * 1e-3 # sec
			freq = float(d[3]) 
			dt_pc = (time.time() - epoch_time) - 1 # Expected 1 sec offset
		except:
			logging.error("Bad " + self.BOX_TYPE + " sync packet: " + str(d))
			return
		
		logging.debug("Sync (" + str(self._port) + "): " + str(syncnum) + " " + str(d[1]) + " " + str(d[2]) + "ms " + str(d[3]) + "Hz")
		
		if abs(dt_pc) > 500:
			logging.error("Catastrophic mismatch between PC time and IRIG time (%0.1f sec)" % dt_pc)
		elif abs(dt_pc) > 5:
			logging.warning("PC time is de-syncronized from IRIG time (%0.1f sec)" % dt_pc)
		
		if syncnum is not None:
			self.get_sensor(id_, Sensor.TYPE_NUMBER).set_value(syncnum, epoch_time, sync_num=None)
		self.get_sensor(id_, Sensor.TYPE_FREQUENCY).set_value(freq, update_time, sync_num=syncnum)
		self.get_sensor(id_, Sensor.TYPE_TIME).set_value(dt_sync, update_time, sync_num=syncnum)
			
	
	def handle_packet_sensor(self, update_time, sync, d):

		# All valid data packets have exactly 4 fields
		if len(d) != 3:
			logging.error("Bad " + self.BOX_TYPE + " message (wrong field count): " + str(d))
			return
		
		# Unpack the fields
		id_, val, exc = d
		
		chan = self.get_channel(id_)
		calib_func = self.get_calib_func(id_)
		
		if chan is None:
			logging.debug(self.BOX_TYPE + " channel reported but not saved: " + id_)
			return
		
		try:
			val = float(val)
			exc = float(exc) 
		except:
			logging.error("Bad " + self.BOX_TYPE + " message (failed field extraction): " + str(d))
			return
		
		c_type = chan['type']
		
		if (c_type in [Sensor.MULTI_TYPE_HEATER_P_I, Sensor.MULTI_TYPE_HEATER_T_P_I]):
			
			val_i = val * 1e-6 # Amps
			val_v = exc # Volts
			val_r = chan.get('r_heater', 0)
			val_p = val_i**2 * val_r # Watts
			
			self.get_sensor(id_, Sensor.TYPE_VOLTAGE).set_value(val_v, update_time, sync)
			
			# Voltage updates will send a NaN for the current
			# (to ack a voltage change without waiting for a reading)
			if not math.isnan(val_i):
				self.get_sensor(id_, Sensor.TYPE_CURRENT).set_value(val_i, update_time, sync)
				self.get_sensor(id_, Sensor.TYPE_POWER).set_value(val_p, update_time, sync)					
			
		elif (c_type == Sensor.MULTI_TYPE_THERM_R_EXC):
			
			val_r = val # Ohms 
			try:
				val_t = calib_func(val_r) # K
			except ValueError:
				val_t = 0
			val_i = exc * 1e-6 # Amps
			val_p = val_r * val_i**2
			
			self.get_sensor(id_, Sensor.TYPE_RESISTANCE).set_value(val_r, update_time, sync)
			self.get_sensor(id_, Sensor.TYPE_TEMPERATURE).set_value(val_t, update_time, sync)
			self.get_sensor(id_, Sensor.TYPE_CURRENT).set_value(val_i, update_time, sync)
			#~ self.get_sensor(id_, Sensor.TYPE_POWER).set_value(val_p, update_time, sync)
					
		elif (c_type == Sensor.MULTI_TYPE_THERM_DIODE_EXC):
			
			val_v = val # Volts 
			try:
				val_t = calib_func(val_v) # K
			except ValueError:
				val_t = 0
			val_i = exc * 1e-6 # Amps
			val_p = val_i * val_v
			
			self.get_sensor(id_, Sensor.TYPE_VOLTAGE).set_value(val_v, update_time, sync)
			self.get_sensor(id_, Sensor.TYPE_TEMPERATURE).set_value(val_t, update_time, sync)
			self.get_sensor(id_, Sensor.TYPE_CURRENT).set_value(val_i, update_time, sync)
			#~ self.get_sensor(id_, Sensor.TYPE_POWER).set_value(val_p, update_time, sync)
					
		elif (c_type == Sensor.TYPE_CURRENT):

			# Currents are reported in uA but stored in A
			val *= 1e-6 #  Amps
			self.get_sensor(id_, c_type).set_value(val, update_time, sync)
			
		elif (c_type in Sensor.VALID_TYPES):

			self.get_sensor(id_, c_type).set_value(val, update_time, sync)
		
		elif isinstance(c_type, list) and (len(c_type) == 2) and (Sensor.TYPE_VOLTAGE in c_type):
				
			# Manual dual type, voltage to something else.
			# Let's figure out the target type.
			target_type = c_type[0]
			if (target_type == Sensor.TYPE_VOLTAGE):
				target_type = c_type[1]
			
			val_conv = self.get_calib_func(id_)(val)
			
			self.get_sensor(id_, Sensor.TYPE_VOLTAGE).set_value(val, update_time, sync)
			self.get_sensor(id_, target_type).set_value(val_conv, update_time, sync)
			
		elif isinstance(c_type, list) and (len(c_type) == 2) and (Sensor.TYPE_CURRENT in c_type):
				
			# Manual dual type, current to something else.
			# Let's figure out the target type.
			target_type = c_type[0]
			if (target_type == Sensor.TYPE_CURRENT):
				target_type = c_type[1]
			
			# Currents are reported in uA but stored in A
			val *= 1e-6 #  Amps
			
			val_conv = self.get_calib_func(id_)(val)
			
			self.get_sensor(id_, Sensor.TYPE_CURRENT).set_value(val, update_time, sync)
			self.get_sensor(id_, target_type).set_value(val_conv, update_time, sync)
		
		else:
			logging.error("Sensor type '" + str(c_type) + "' not handled during data receive for " + self.BOX_TYPE + " channel " + str(id_))
		
					
	def set_excitation(self, chan_id, value):
		self.send_packet("eset,%s,%s" % (chan_id, value))
	
	def set_pid_param(self, chan_id, pid_therm, pid_p, pid_i, pid_d):
		self.send_packet("pidparam,%s,%s,%0.5g,%0.5g,%0.5g" % (chan_id, pid_therm, pid_p, pid_i, pid_d))
	
	# Implements VoltageOutputMixin.set_temperature
	def set_temperature(self, chan_id, value):
		
		if value is None:
			logging.debug("Disabling " + self.BOX_TYPE + " " + str(chan_id) + " PID")
			self.send_packet("pidoff,%s" % (chan_id))
			return

		# Check if we can find the thermometer.  If not, this 
		# will default to a calib func that returns 0.
		therm_id = self._pid_therm.get(chan_id)
		func = self.get_calib_func(therm_id)
		
		# Try to find the current value as a guess for what resistance
		# is resonable for this sensor
		sen = self.get_sensor(therm_id, Sensor.TYPE_RESISTANCE, none_on_fail=True)
		if (sen is not None) and (np.isfinite(sen.value)) and (sen.value > 0):
			r_initial = sen.value
		else:
			r_initial = 1000 # Ohm
		logging.debug("Using an initial resistance guess of %0.3f Ohm to invert the calibration function for PID calculations" % r_initial)

		# Invert the calib function at this point
		f_root = lambda x: func(x) - value
		try:
			# x0 and x1 are two guesses for root_scalar to try 
			r = optimize.root_scalar(f_root, x0=r_initial, x1=r_initial+1).root
		except:
			logging.error("Failed to invert temperature calibration for PID settings!")
			r = 0
		
		# Find the first derivative, used approximate T(R)
		try:
			drdt = 1.0 / derivative(func, r)
		except:
			drdt = 0
		
		logging.debug("Setting " + self.BOX_TYPE + " " + str(chan_id) + " PID at r = " + str(r) + ", which is T = " + str(func(r)) + " and dR/dT = " + str(drdt))
		
		self.send_packet("pidon,%s,%0.5g,%0.5g,%0.5g" % (chan_id, value, r, drdt))
		
	# Implements VoltageOutputMixin.set_voltage
	def set_voltage(self, chan_id, value):
		
		if chan_id[0] != 'H':
			logging.error("Tried to set voltage on invalid channel: " + str(chan_id))
			return
	
		# Extract the heater number
		try:
			h = int(chan_id[1:])
			if (h < 0) or (h >= 32):
				raise ValueError
		except (ValueError, IndexError, TypeError):
			logging.error("Tried to set voltage on invalid channel: " + str(chan_id))
			return
		
		logging.debug("Requesting voltage %.4f on channel %i" % (value, h))
		self.send_packet("vset,%i,%.4f" % (h, value))
	
	# Load a json file with the live config
	def execute_live_config(self):
		
		if self._cfg_live is None:
			return
		
		config = self._cfg_live.load()
		
		if config is None:
			logging.error("Unable to load HKMB config file: " + str(self._cfg_live_filename))
			self.fix_live_config()
			return
		
		try:
			for c in config:
				c_id = c['id']
				c_type = c['type']
				if c_type == "thermometer":	
					c_exc = c['excitation']
					self.set_excitation(c_id, c_exc)
				elif c_type == "heater":	
					pid_therm = c['pid_therm']
					self._pid_therm[c_id] = pid_therm
					self.set_pid_param(c_id, pid_therm, c['pid_p'], c['pid_i'], c['pid_d'])
				
			logging.info("Successfully reloaded " + self.BOX_TYPE + " live config: " + str(self._cfg_live_filename))	
			
		except (ValueError, TypeError):
			logging.error(str(traceback.format_exc()).rstrip())
			logging.error("Contained error while interpreting config file: " + str(self._cfg_live_filename))
			self.fix_live_config()
			
	# Generates and returns a default live config file
	def default_live_config(self):
		
		default_config = []
		
		# Metadata
		chan_cfg = OrderedDict()
		chan_cfg['id'] = 'meta'
		chan_cfg['type'] = 'meta'
		chan_cfg['name'] = 'meta'
		chan_cfg['version'] = self.BOX_TYPE			
		chan_cfg['port'] = self._port			
		chan_cfg['serial_num'] = self._serial_num			
		default_config.append(chan_cfg)
		
		# Thermometers
		for i in list(range(9)) + ['T']:
			for j in range(4):
				id_ = 'T' + str(i) + str(j)
				chan = self.get_channel(id_)
				
				if chan is None:
					continue
				
				chan_cfg = OrderedDict()
				chan_cfg['id'] = id_
				chan_cfg['type'] = 'thermometer'
				
				if 'alias' in chan:
					chan_cfg['alias'] = chan['alias']
				
				chan_cfg['name'] = chan.get('name','MISSING')
				chan_cfg['excitation'] = 10000			
					
				default_config.append(chan_cfg)
		
		# Heaters
		for i in range(32):
			id_ = 'H%02i' % i
			chan = self.get_channel(id_)
			
			if chan is None:
				continue
			
			chan_cfg = OrderedDict()
			
			chan_cfg['id'] = id_
			chan_cfg['type'] = 'heater'
			chan_cfg['name'] = chan.get('name','MISSING')
			
			if 'alias' in chan:
				chan_cfg['alias'] = chan['alias']
				
			chan_cfg['pid_p'] = 0
			chan_cfg['pid_i'] = 0
			chan_cfg['pid_d'] = 0
			chan_cfg['pid_therm'] = None

			default_config.append(chan_cfg)
		
		return default_config
			
	# Update the live config with current themometer names and ensure 
	# all channels are represented.  Eject any malformed channels.
	def fix_live_config(self):
		
		if self._cfg_live is None:
			return
			
		logging.info("Checking and fixing " + self.BOX_TYPE + " live config file: " + str(self._cfg_live_filename))
	
		old_config = self._cfg_live.load()
		new_config = self.default_live_config()
		
		if old_config is None or not isinstance(old_config, list):
			logging.error("Empty or bad " + self.BOX_TYPE + " live config file, generating default: " + str(self._cfg_live_filename))
		else:
			# Start from a good default with current names,
			# and then copy over any keys from valid entries in the
			# old config
			for oc in old_config:
				id_ = oc.get('id', None)
				if id_ is None:
					continue
				for nc in new_config:
					if id_ == nc['id']:
						for ok in list(oc.keys()):
							# Don't transfer the old name in case it changed
							if ok not in ['name', 'alias']:
								nc[ok] = oc[ok]
						continue
		
		self._cfg_live.dump(new_config)

	# Environmental sensors
	def handle_packet_env(self, update_time, syncnum, d):

		id_ = "ENV"
		
		if id_ not in self.sensor_ids:
			return

		try:
			val_t_celcius = float(d[0])
			val_t = val_t_celcius + 273.15 # K
			val_p = float(d[1]) * 0.00750062 # Torr
			val_h = float(d[2]) # %
		except:
			logging.error("Bad HKMB env packet: " + str(d))
			return

		self.get_sensor(id_, Sensor.TYPE_TEMPERATURE).set_value(val_t, update_time, syncnum)
		self.get_sensor(id_, Sensor.TYPE_PRESSURE).set_value(val_p, update_time, syncnum)
		self.get_sensor(id_, Sensor.TYPE_RELHUMIDITY).set_value(val_h, update_time, syncnum)
		
		id_ = "DEW"
		
		if id_ not in self.sensor_ids:
			return

		try:
			# https://en.wikipedia.org/wiki/Dew_point
			b = 18.678
			c = 257.14
			gamma = math.log(val_h/100) + b * val_t_celcius / (c + val_t_celcius)
			val_dew = c * gamma / (b - gamma) + 273.15
		except (ZeroDivisionError, ValueError):
			val_dew = float('nan')

		self.get_sensor(id_, Sensor.TYPE_TEMPERATURE).set_value(val_dew, update_time, syncnum)
			
