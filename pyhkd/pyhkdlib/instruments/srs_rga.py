import time
import logging
import serial
import struct
import numpy as np

from pyhkdlib.sensor import Sensor
from .serial_instrument import SerialInstrument
from calib.helpers import get_calib

# SRS RGA100 Residual Gas Analyzer
#
# Note that the RGA100 has some serial communication quirks that make it
# hard to communicate elegantly.  It uses ASCII and line endings for
# most communication, but it switches to binary data without packets
# delimiters for scan data.
#

# From the SRS RGA control application.  Fractions in decreasing order.
frag_patterns = {
	"H20": ([18, 17, 16, 20, 19], [74.40, 17.11, 8.18, 0.223, 0.074]),
	"O2": ([32, 16, 34, 33], [89.37, 10.19, 0.357, 0.089]),
	"N2": ([28, 14, 29], [92.59, 6.67, 0.74]),
	# ~ "CO": ([28, 12, 16, 29, 14, 30], [91.58, 4.58, 1.83, 0.916, 0.916, 0.183]), # Mostly degenerate with N2
	"H2": ([2, 1], [95.24, 4.76]),
	"He": ([4], [100]),
	"CO2": ([44, 28, 16, 12, 45, 46], [78.43, 8.63, 7.06, 4.71, 0.784, 0.392]),
}
amu_mask = [3,5,6] # Masking out spill-over from He so it doesn't end up in Other
for a in amu_mask:
	frag_patterns['mask_%iamu' % a] = ([a],[100])
frag_peaks = {v[0][0]: k for k, v in frag_patterns.items()}
assert len(frag_peaks.keys()) == len(set(frag_peaks.keys())), 'RGA fragmentation pattern peaks are degenerate'

class SRSRGA(SerialInstrument):
	
	NUM_SENSORS = 0
	BOX_TYPE = 'SRSRGA'
	
	# This device has a dedicated serial connection and often responds
	# very slowly.
	SER_ASK_TIMEOUT = 20
	SER_PACKET_TIMEOUT = 100 # Allow very long data packets for slow scans
			
	# Uses a weird and fixed baud rate of 28800
	def __init__(self, port, channels=[], wait_time=2, baudrate = 28800, channel_prefix = 'RGA', **kwargs):
		
		self._noise_floor = 2 # 0 to 7, with 0 being the lowest (and slowest)
		self._amu_max = 64
		self._amu_min = 1
		self._data_pkt_size = (self._amu_max - self._amu_min + 2) * 4
		self._cal_partial = 0 # mA/Torr, loaded from device
		self._cal_total = 0 # mA/Torr, loaded from device
		self._abort_time = 200 # sec before aborting and flushing a waiting hist
		self._renable_time = 200 # sec offline before resetting
		
		assert len(channels) == 0, 'The RGA config uses "channel_prefix" instead of "channels"'
		
		# Generate the list of channels
		p = str(channel_prefix)
		channels.append({"id": "total", "name": p+" Total Pressure", "type": Sensor.TYPE_PRESSURE})
		for cid in range(self._amu_min, self._amu_max+1):
			cid_str = '%03i' % cid
			channels.append({"id": cid_str, "name": p+" "+cid_str+" amu", "type": Sensor.TYPE_PRESSURE})
		for cid in list(frag_patterns.keys()) + ['Other']:
			channels.append({"id": cid, "name": p+" "+cid, "type": Sensor.TYPE_FRACTION})
		self.NUM_SENSORS = len(channels)
		
		SerialInstrument.__init__(self, 
			port, 
			baudrate = baudrate, 
			bytesize=serial.EIGHTBITS, 
			parity=serial.PARITY_NONE, 
			stopbits=serial.STOPBITS_ONE,
			pkt_end = '\r',
			pkt_start = None,
			channels = channels,
			default_sensor_type = Sensor.TYPE_PRESSURE, 
			verbose_fail = True,
			verbose_rx = False,
			return_bytes = False,
			fixed_rx_size = None,
			**kwargs)
		
		self.wait_time = wait_time		
		self._online = False
		self._hist_waiting = False
		self._num_fail = 0
		self.rga_enable()
		
	def rga_enable(self):
		
		self.rx_settings(pkt_end='\r', pkt_start=None, return_bytes=False, fixed_rx_size=None)
		
		# Interrupt any ongoing continuous scans
		self.purge_tx_buf()
		self.send_packet('HS0')		
		self.purge_rx_buf()
		
		self._num_fail = 0
		self._online = False
		self._hist_waiting = False
		
		# Get the ID number to check comms
		self.send_packet('ID?', resp_callback=self.handle_id)	
		
		# Reset to factory defaults and check status
		self.send_packet('IN1', resp_callback=self.handle_reset)	
		
		# Read the calibration values
		self.send_packet('SP?', resp_callback=self.handle_calib_partial)	
		self.send_packet('ST?', resp_callback=self.handle_calib_total)	
	
	# Returns true if the status is good.  Returns False and marks as
	# offline if status is bad.
	def status_ok(self, msg):
		
		result = str(msg)
		try:
			result = int(result.rstrip())
		except:
			pass
		
		if result == 0:
			logging.debug("RGA status: OK")
			return True
		else:
			logging.info("RGA status: " + msg)
			self._online = False
			return False
	
	def _extract_float(self, msg):
		try:
			return float(msg.strip())
		except:
			return 0.0
	
	def handle_calib_partial(self, msg):
		self._cal_partial = self._extract_float(msg)
		logging.debug("RGA partial pressure sensitivity: %0.3g mA/Torr" % self._cal_partial)
		
	def handle_calib_total(self, msg):
		self._cal_total = self._extract_float(msg)
		logging.debug("RGA total pressure sensitivity: %0.3g mA/Torr" % self._cal_total)
		
	def handle_id(self, msg):
		logging.debug("RGA ID: " + str(msg).rstrip())
		
	# Handle histogram scan results
	def handle_hist(self, msg):
		logging.debug("RGA hist scan: " + str(msg).rstrip())
		
	def handle_reset(self, msg):
		
		if not self.status_ok(msg):
			return
		
		self._online = True
		logging.debug("RGA online")
		
		# Turn everything on with default values, and set up the 
		# histogram scan
		for p in ['EE*', 'IE*', 'VF*', 'FL*']:
			self.send_packet(p, resp_callback=self.status_ok)	
	
	# Program a histogram scan (integer masses)
	def start_hist(self):
		
		if self._hist_waiting:
			logging.debug("Attempted to request new scan before current one finished, ignoring")
			return
				
		self._hist_waiting = True
		
		# The RGA does not transmit anything to let us know a data
		# packet has ended, so we need to rely on packet size. I really
		# don't like this.
		self.rx_settings(pkt_end=None, pkt_start=None, return_bytes=True, fixed_rx_size=self._data_pkt_size)
		self.purge_tx_buf()
		self.purge_rx_buf()
		
		logging.debug("Requesting RGA histogram scan")
		for p in ['MI%i' % self._amu_min, 'MF%i' % self._amu_max, 'NF%i' % self._noise_floor, 'HS1']:
			self.send_packet(p + '\r') # Manually add ending since we just disabled it
	
	# Handle the histogram output
	def handle_packet(self, msg):
		
		if len(msg) != self._data_pkt_size:
			logging.error("Unexpected RGA data packet size: %i" % len(msg))
			return
		
		# ~ logging.debug(msg)
		
		# Convert to mA
		current_mA = [x[0] * 1.0e-13 for x in struct.iter_unpack('<l', msg)]
		
		# ~ logging.debug(current_mA)
		
		# Convert to Torr
		partial = np.asarray(current_mA[:-1]) / self._cal_partial
		total = current_mA[-1] / self._cal_total
		
		update_time = time.time()
		
		# Save the pressures
		for cid in range(self._amu_min, self._amu_max+1):
			s = self.get_sensor('%03i' % cid, Sensor.TYPE_PRESSURE)
			s.set_value(partial[cid-1], update_time)
		s = self.get_sensor('total', Sensor.TYPE_PRESSURE)
		s.set_value(total, update_time)
		
		partial_resid = np.copy(partial)
		
		# Use the main peaks to find the gas fractions (assuming no
		# degeneracy and that we are looking at ~air)
		partial_gas = {}
		for gas in frag_patterns.keys():
			
			frag_amu, frag_frac = frag_patterns[gas]
			peak_amu = frag_amu[0]
			
			# Normalize
			frag_frac = np.asarray(frag_frac) / sum(frag_frac)
			
			# Find the gas total partial pressure from just the peak
			partial_gas[gas] = partial[peak_amu-1] / frag_frac[0]
			
			# Subtract the other fragments from the residuals
			for i in range(len(frag_amu)):
				a = frag_amu[i] - 1
				partial_resid[a] -= partial_gas[gas] * frag_frac[i]
				partial_resid[a] = max(0, partial_resid[a])
		
		# Bin any leftovers	
		partial_gas['Other'] = sum(partial_resid)
		
		# Sum everything except the masked values
		partial_total = 0
		for gas, val in partial_gas.items():
			if 'mask' not in gas:
				partial_total += val
		
		# Save the gas fractions
		for gas, val in partial_gas.items():
			frac = val / partial_total
			s = self.get_sensor(gas, Sensor.TYPE_FRACTION)
			s.set_value(frac, update_time)
			logging.debug(gas + ": " + str(frac))
			
		logging.debug("RGA total pressure at %0.3g Torr" % total)
		logging.debug("RGA partial pressure sum at %0.3g Torr" % sum(partial))
		
		self._hist_waiting = False
		self._num_fail = 0
	
	
	def update(self):
		
		if not self._online:
			
			# Re-initialize if we have been offline long enough
			if ((time.time() - self.last_update_time) >= self._renable_time):
				self.last_update_time = time.time()
				self.rga_enable()
				
			# Nothing else matters
			return
		
		# Don't request anything if we are still communicating the
		# initial settings
		if (self.tx_buf_len > 0) or self.ask_waiting:
			return	
		
		if not self._hist_waiting:
			
			# Request a new histogram
			if ((time.time() - self.last_update_time) >= self.wait_time):
				self.start_hist()
				self.last_update_time = time.time() # (We still want this even if we fail, so it gives time before retrying)
		
		else:
			
			# Abort if we haven't heard back in awhile
			if ((time.time() - self.last_update_time) >= self._abort_time):
				if self.tx_buf_len == 0:
					logging.error("Never heard back on RGA histogram request, clearing")
					self.purge_rx_buf()
					self._hist_waiting = False
					self._num_fail += 1
					if self._num_fail > 5:
						self._online = False
		
