

##### PyHK Fridge Remote Settings #####
 
DATA_LOG_FOLDER = '/data/hk'
PYHKFRIDGE_PROCNAME = 'pyhkfridge'
FRIDGE_SCRIPT_FOLDER = '../pyhkfridge/scripts'
FRIDGE_STATE_BASE_FOLDER = '/data/hk/tmp/pyhkfridge'
FRIDGE_LOG_BASE_FOLDER = '/data/hk'
PYHKFRIDGE_IP = "localhost"
PYHKFRIDGE_BASE_PORT = 8400
PYHKFRIDGE_MAX_PORT = PYHKFRIDGE_BASE_PORT + 10
